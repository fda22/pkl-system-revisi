<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sekolah_dummy extends Model
{
    public $table = 'sekolah_dummy';

    protected $fillable = ['id_sekolah','nama_sekolah', 'foto_sekolah', 'alamat', 'nama_pembimbing', 'no_tlp', 'email', 'password'];
    protected $guarded = ['id_sekolah'];

    protected $primaryKey = "id_sekolah";

    public function user(){
	  	return $this->belongsTo(User::class,'id_sekolah','id_sekolah');
	  }
}