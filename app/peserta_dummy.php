<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class peserta_dummy extends Model
{
	public $table = 'peserta_dummy';

	protected $fillable = ['foto','no_tlp','username','password','nama','sekolah','tanggal_masuk','tanggal_keluar', 'status'];
	protected $guarded = ['NISN'];

	protected $primarykey = ['NISN'];  
	
	public $timestamps = false;

	function peserta_dummy(){
		return $this->belongsTo(User::class,'NISN', 'NISN');
	}
	function kehadiran_dummy()
	{
		return $this->hasOne(kehadiran_dummy::class, 'NISN', 'NISN');
	}
	function penilaian_dummy()
	{
		return $this->hasOne(penilaian::class, 'NISN', 'NISN');
	}
	function datatugas()
	{
		return $this->hasOne(DataTugas::class, 'NISN', 'NISN');
	}

}
