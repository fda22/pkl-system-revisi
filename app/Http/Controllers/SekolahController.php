<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Helper\Image;
use App\User;
use App\peserta_dummy;
use App\sekolah_dummy;
use Carbon\Carbon;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class SekolahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     //sekolah
    
    public function indexSekolah(Request $request)
    {
        $user = Auth::user();
            $cari = $request->nama_sekolah;
            $datapeserta = DB::table('peserta_dummy')
                ->where('sekolah','like',"%".$cari."%")
                ->paginate(5);
            // $datasekolah = sekolah_dummy::with('sekolah_dummy')->get();
            // $datasekolah = User::where('id_sekolah', $user->id_sekolah)->get();
            $sklh = DB::table('sekolah_dummy')->get();
            $totalPeserta = count($datapeserta);
            $totalSklh = count($sklh);
        
        // Contoh akses fungsi belongs to ke fungsi user dan mengambil data emailnya
        // foreach($datasekolah as $item) {
        //     var_dump($item->user->email);
        // }
        // dd($user->id_sekolah);
            
        return view('templateSekolah.page.home', [
            "user" => $user,
            "totalPeserta" => $totalPeserta,
            "totalSklh" => $totalSklh,
            'datapeserta' => $datapeserta,
            // 'datasekolah' => $datasekolah,
            'cari' => $cari
        ]);
    }
    public function edit_profile_sekolah() {
        $user = Auth::user();
        return view('templateSekolah.page.editProfile',[
            "user" => $user
        ]);
    }
    public function dataPesertacari(Request $request)
    {
        $user = Auth::user();
        // menangkap data pencarian
        $cari = $request->cari;
 
            // mengambil data dari table pegawai sesuai pencarian data
        $datapeserta = DB::table('peserta_dummy')
        ->where('nama','like',"%".$cari."%")
        ->paginate(5);
 
            // mengirim data pegawai ke view index
        return view('templateSekolah.page.home', [
            "user" => $user
            ,'datapeserta' => $datapeserta,
            'cari' => $cari
        ]);
    }
    public function tambahPeserta()
    {
        $user = Auth::user();
        $datasekolah = DB::table('sekolah_dummy')->get();
        return view('templateSekolah.page.tambahpeserta', [
            "user" => $user,
            'datasekolah' => $datasekolah
        ]);
    }
   public function tambahdatapeserta(Request $request)
    {
        $request->validate([
            'nisn'          => 'required|numeric|unique:peserta_dummy,NISN|min:10',
            'namapeserta'   => 'required|string',
            'notlp'         => 'required|numeric|min:10',
            'password'      => 'required',
            'sekolah'       => 'required',
            'email'         => 'required|email',
            'tanggalmasuk'  => 'required|date',
            'tanggalkeluar' => 'required|date',
            'foto'          => 'required|image'

        ]);

        $nisn           = $request->input('nisn');
        $foto           = $request->file('foto');
        $extension      = $foto->extension();
        $no_tlp         = $request->input('notlp');
        $password       = $request->input('password');
        $password1      = Hash::make($request->input('password'));
        $namapeserta    = $request->input('namapeserta');
        $sekolah        = $request->input('sekolah');
        $email          = $request->input('email');
        $tanggalmasuk   = $request->input('tanggalmasuk');
        $tanggalkeluar  = $request->input('tanggalkeluar');
        $admin          = $request->input('admin');
        $status         = $request->input('status');
        $created        = Carbon::now()->toDateTimeString();

        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_'.$request->foto->getClientOriginalName();
            $pathToStore = $request->file('foto')->move('foto',$fileNameToStore);

        $data=array('NISN'=>$nisn,
            "foto"=>$pathToStore,
            'no_tlp'=>$no_tlp,
            "password"=>$password,
            "nama"=>$namapeserta,
            "sekolah"=>$sekolah,
            "email"=>$email,
            "tanggal_masuk"=>$tanggalmasuk,
            "tanggal_keluar"=>$tanggalkeluar,
            "status" =>$status,
            "created_at"=>$created);
        $data2=array(
            "NISN" => $nisn
        );
        DB::table('penilaian')->insert([$data2]);
        $data1=array('NISN'=>$nisn,
            "password"=>$password1,
            "name"=>$namapeserta,
            "email"=>$email,
            "admin"=>$admin);
        DB::table('users')->insert([$data1]);
        
        
        DB::table('peserta_dummy')->insert([$data]);
        return redirect('/sekolah/Home')->with(['success' => 'Data berhasil dimasukkan']);
    }
    public function editPeserta($id)
    {
        $user = Auth::user();
        $datapeserta = DB::table('peserta_dummy')->where('NISN',$id)->get();
        return view('templateSekolah.page.editPeserta', [
            "user" => $user
            ,'datapeserta' => $datapeserta
        ]);

    }
   public function editPesertaFungsi(Request $request, $NISN)
    {
        $request->validate([
            'namapeserta'   => 'required|string',
            'notlp'         => 'required|numeric|min:10',
            'password'      => 'required',
            'sekolah'       => 'required',
            'email'         => 'required|email',
            'tanggalmasuk'  => 'required|date',
            'tanggalkeluar' => 'required|date',
            'foto'          => 'required|image'

        ]);
        $gambar = peserta_dummy::where('NISN',$NISN)->first();
        File::delete($gambar->foto);

        $nisn           = $request->input('nisn');
        $foto           = $request->file('foto');
        $extension      = $foto->extension();
        $no_tlp         = $request->input('notlp');
        $password       = $request->input('password');
        $password1      = Hash::make($request->input('password'));
        $namapeserta    = $request->input('namapeserta');
        $sekolah        = $request->input('sekolah');
        $email          = $request->input('email');
        $tanggalmasuk   = $request->input('tanggalmasuk');
        $tanggalkeluar  = $request->input('tanggalkeluar');

        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_'.$request->foto->getClientOriginalName();
            $path = $request->file('foto')->storeAs('foto',$fileNameToStore);
            $pathToStore = copy($request->file('foto'), "foto/$fileNameToStore");
        DB::table('peserta_dummy')->where('NISN','=',$nisn)->update([
            "foto"      =>$path,
            'no_tlp'    =>$no_tlp,
            "password"  =>$password,
            "nama"      =>$namapeserta,
            "sekolah"   =>$sekolah,
            "email"     =>$email,
            "tanggal_masuk"=>$tanggalmasuk,
            "tanggal_keluar"=>$tanggalkeluar
        ]);
         DB::table('users')->where('NISN','=',$nisn)->update([
            "password"  =>$password1,
            "name"      =>$namapeserta,
            "email"     =>$email
        ]);
        return redirect('/sekolah/Home')->with(['success' => 'Data berhasil diubah']);
    }

    public function edit_sekolah_fungsi(Request $request)
    {
        // dd($request);
        $request->validate([
            'foto.*' => 'mimes:jpg,jpeg,png,jpg,gif|max:2048'

        ]);

        $user               = Auth::user();
        $NPSN               = $request->input('npsn');
        $nama_sekolah       = $request->input('nama_sekolah');
        $alamat             = $request->input('alamat');
        $nama_pembimbing    = $request->input('nama_pembimbing');
        $no_tlp             = $request->input('no_telp');
        $foto               = $request->file('foto');
        $extension          = $foto->extension();
        $email              = $request->input('email');
        $password           = $request->input('password');
        $password1          = Hash::make($request->input('password'));
        $admin              = $request->input('admin');

        // $gambar = sekolah_dummy::where('id_sekolah',$NPSN)->first();
        // File::delete($gambar->foto_sekolah);
        
        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_sklh_'.$request->foto->getClientOriginalName();
            $path = $request->file('foto')->storeAs('foto',$fileNameToStore);
            $pathToStore = copy($request->file('foto'), "foto/$fileNameToStore");

        DB::table('sekolah_dummy')->where('id_sekolah','=',$NPSN)->update([
            "nama_sekolah"=>$nama_sekolah,
            "alamat_sekolah"=>$alamat,
            "nama_pembimbing"=>$nama_pembimbing,
            "no_tlp"=>$no_tlp,
            "foto_sekolah"=>$path,
            "email"=>$email,
            "password"=>$password
        ]);

        DB::table('users')->where('id_sekolah','=',$NPSN)->update([
            "password"  =>$password1,
            "name"      =>$nama_sekolah,
            "email"     =>$email
        ]);

    
            return redirect('/sekolah/Home');
    }

    public function hapusPeserta(Request $request, $NISN){
    $gambar = peserta_dummy::where('NISN',$NISN)->first();
    File::delete($gambar->foto);
    
    peserta_dummy::where('NISN',$NISN)->delete();
         return redirect('/sekolah/Home')->with(['success' => 'Data berhasil dihapus']);
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
    
}
