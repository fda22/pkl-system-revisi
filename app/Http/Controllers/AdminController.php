<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\MetodePembayaran;
use App\JasaPengiriman;
use App\DetailMetodePembayaran;
use App\KategoriBarang;
use App\Barang;
use App\FotoBarang;
use App\peserta_dummy;
use App\sekolah_dummy;
use App\DataTugas;
use App\kehadiran_dummy;
use App\penilaian;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use File;
use PDF;
use Carbon\Carbon;
use App\Http\Controllers\Helper\Image;
use App\Http\Controllers\Helper\FotoBarangHelper;
use App\Http\Controllers\Helper\Pembayaran;
use App\Mail\diterima;
use App\Mail\ditolak;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function accepted(Request $request)
    {
        $accept = $request->input('check');
        $status = $request->update;

        if ($status == 'Di Terima') {
            peserta_dummy::whereIn('NISN',$accept)->update([
            'status' => $status
         ]);
            $email = DB::table('peserta_dummy')->whereIn('NISN',$accept)->get('email');
            foreach ($email as $mail) {
            Mail::to($mail)->send(new diterima());
            }   
        } else{
            DB::table('peserta_dummy')->whereIn('NISN',$accept)->update([
            'status' => $status
         ]);

            $email = DB::table('peserta_dummy')->whereIn('NISN',$accept)->get('email');
            foreach ($email as $mail) {
                Mail::to($mail)->send(new ditolak());
            }
        }
        return back()->with(['success' => 'E-mail berhasil dikirim']);
    }

    public function diterima(Request $request, $NISN)
    {
        $update = $request->update_data_peserta;

        DB::table('peserta_dummy')->where('NISN',$NISN)->update([
        'status' => $update
         ]);
        $email = DB::table('peserta_dummy')->where('NISN',$NISN)->get('email');
        Mail::to($email)->send(new diterima());

        return back()->with(['success' => 'E-mail berhasil dikirim']);
    }

    public function tidakditerima(Request $request, $NISN)
    {
        $update = $request->update_data_peserta;

        DB::table('peserta_dummy')->where('NISN',$NISN)->update([
        'status' => $update
         ]);

        $email = DB::table('peserta_dummy')->where('NISN',$NISN)->get('email');
        Mail::to($email)->send(new ditolak());

        return back()->with(['success' => 'E-mail berhasil dikirim']);
    }

        public function kirimpdf()
    {
        $datapeserta = peserta_dummy::all();

        $pdf = PDF::loadview('peserta_pdf',['datapeserta'=>$datapeserta]);
        return $pdf->download('data-peserta-pdf');
    }

    public function indexAdmin()
    {
        $user = Auth::user();
        $month = Carbon::now()->month;
        $pesertatugas = peserta_dummy::where('status','=','Di Terima')->orderBy('sekolah','asc')->paginate(5);
        $pesertabelom = peserta_dummy::where('status','=','Belum Di Tentukan')->orderBy('sekolah','asc')->paginate(5);

            $peserta = DB::table('peserta_dummy')->get();
            $totalPeserta = count($peserta);
            $sklh = DB::table('sekolah_dummy')->get();
            $totalSklh = count($sklh);

            $pesertabulan = $peserta = DB::table('peserta_dummy')->whereMonth('created_at', $month)->get();
            $totalpesertabulan = count($pesertabulan);
            $diterima = DB::table('peserta_dummy')->whereMonth('created_at', $month)->where('status','=','Di Terima')->get();
            $totalditerima = count($diterima);
            $tidakditerima = DB::table('peserta_dummy')->whereMonth('created_at', $month)->where('status','=','Di Tolak')->get();
            $totaltidakditerima = count($tidakditerima);
            $belumditentukan = DB::table('peserta_dummy')->whereMonth('created_at', $month)->where('status','=','Belum Di Tentukan')->get();
            $totalbelumditentukan = count($belumditentukan);

            if ($month == '01') {
                $month = "Januari";
                }elseif ($month == '02') {
                    $month = "Februari";
                }elseif ($month == '03') {
                    $month = "Maret";
                }elseif ($month == '04') {
                    $month = "April";
                }elseif ($month == '05') {
                    $month = "Mei";
                }elseif ($month == '06') {
                    $month = "Juni";
                }elseif ($month == '07') {
                    $month = "Juli";
                }elseif ($month == '08') {
                    $month = "Agustus";
                }elseif ($month == '09') {
                    $month = "September";
                }elseif ($month == '10') {
                    $month = "Oktober";
                }elseif ($month == '11') {
                    $month = "November";
                }else {
                    $month = "Desember";
                };
                
        return view('templateAdmin.page.home', [
            "pesertabelom" => $pesertabelom,
            "pesertatugas" => $pesertatugas,
            "user" => $user,
            "totalPeserta" => $totalPeserta,
            "totalSklh" => $totalSklh,
            'month' => $month,
            'totalditerima' => $totalditerima,
            'totaltidakditerima' => $totaltidakditerima,
            'totalpesertabulan' => $totalpesertabulan,
            'totalbelumditentukan' => $totalbelumditentukan
        ]);
    }

    // public function indexPeserta() {
    //     $user = Auth::user();
    //     $peserta = DB::table('peserta_dummy')->get();
    //     $totalPeserta = count($peserta);
    //     return view('templateAdmin.page.home', [081328246302
    //         "user" => $user,
    //         "totalPeserta" => $totalPeserta
    //     ]);
    // }
    // public function indexSekolah() {
    //     $user = Auth::user();
    //     $sklh = DB::table('sekolah_dummy')->get();
    //     $totalSklh = count($sklh);
    //     return view('templateAdmin.page.home', [
    //         "user" => $user,
    //         "totalSklh" => $totalSklh
    //     ]);
    // }

    public function dataPeserta(Request $request)
    {

        $namasekul = $request->namasekul;
        $status = $request->status;
        $datapeserta = DB::table('peserta_dummy')->orderBy('sekolah','asc')->paginate(10);
        $datasekolah = DB::table('sekolah_dummy')->get();
        $user = Auth::user();
        
        return view('templateAdmin.page.dataPeserta', [
            "user" => $user,
            "namasekul" => $namasekul,
            "status" => $status,
            'datapeserta' => $datapeserta,
            "datasekolah" => $datasekolah,
        ]);
    }

    public function dataPesertacari(Request $request)
    {
        $user = Auth::user();
        // menangkap data pencarian
        $cari = $request->cari;
 
            // mengambil data dari table pegawai sesuai pencarian data
        $datapeserta = DB::table('peserta_dummy')
        ->where('nama','like',"%".$cari."%")
        ->paginate(10);
 
            // mengirim data pegawai ke view index
        return view('templateAdmin.page.dataPeserta', [
            "user" => $user
            ,'datapeserta' => $datapeserta,
            'cari' => $cari]);
    }
    public function filter(Request $request)
    {
        $user = Auth::user();
        $namasekul = $request->namasekul;
        $status = $request->status;
        $datasekolah = DB::table('sekolah_dummy')->get();

        $datapeserta = peserta_dummy::where('sekolah','like',"%".$namasekul."%")
                                    ->where('status','like',"%".$status."%")
                                    ->paginate(10);

        return view('templateAdmin.page.dataPeserta', [
            "namasekul" => $namasekul,
            "status" => $status,
            "datapeserta" => $datapeserta, 
            "user" => $user,
            "datasekolah" => $datasekolah,
        ]);
    }

    // public function hapusPeserta($id)
    // {
    //     DB::table('peserta_dummy')->where('NISN',$id)->delete();
    //     $user = Auth::user();

    //     return redirect('templateAdmin.page.datapeserta');
    // }

    public function hapusPeserta(Request $request, $NISN){
    $gambar = peserta_dummy::where('NISN',$NISN)->first();
    File::delete($gambar->foto);

    peserta_dummy::where('NISN',$NISN)->delete();
    User::where('NISN',$NISN)->delete();
    kehadiran_dummy::where('NISN',$NISN)->delete();
    penilaian::where('NISN',$NISN)->delete();
         return redirect('/admin/DataPeserta')->with(['success' => 'Data berhasil dihapus']);
    }

    public function tambahdatapeserta(Request $request)
    {
        $request->validate([
            'nisn'          => 'required|numeric|unique:peserta_dummy,NISN|min:10',
            'namapeserta'   => 'required|string',
            'notlp'         => 'required|numeric|min:10',
            'password'      => 'required',
            'sekolah'       => 'required',
            'email'         => 'required|email|unique:peserta_dummy,email',
            'tanggalmasuk'  => 'required|date',
            'tanggalkeluar' => 'required|date',
            'foto'          => 'required|image|max:2048'

        ]);

        $nisn           = $request->input('nisn');
        $foto           = $request->file('foto');
        $extension      = $foto->extension();
        $no_tlp         = $request->input('notlp');
        $password       = $request->input('password');
        $password1      = Hash::make($request->input('password'));
        $namapeserta    = $request->input('namapeserta');
        $sekolah        = $request->input('sekolah');
        $email          = $request->input('email');
        $tanggalmasuk   = $request->input('tanggalmasuk');
        $tanggalkeluar  = $request->input('tanggalkeluar');
        $admin          = $request->input('admin');
        $status         = $request->input('status');
        $created        = Carbon::now()->toDateTimeString();


        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_'.$request->foto->getClientOriginalName();
            $pathToStore = $request->file('foto')->move('foto',$fileNameToStore);

        $data=array('NISN'=>$nisn,
            "foto"=>$pathToStore,
            'no_tlp'=>$no_tlp,
            "password"=>$password,
            "nama"=>$namapeserta,
            "sekolah"=>$sekolah,
            "email"=>$email,
            "tanggal_masuk"=>$tanggalmasuk,
            "tanggal_keluar"=>$tanggalkeluar,
            "status" =>$status,
            "created_at"=>$created);
        $data2=array(
            "NISN" => $nisn
        );
        DB::table('penilaian')->insert([$data2]);
        $data1=array('NISN'=>$nisn,
            "password"=>$password1,
            "name"=>$namapeserta,
            "email"=>$email,
            "admin"=>$admin);
        DB::table('users')->insert([$data1]);
        
        DB::table('peserta_dummy')->insert([$data]);
        return redirect('/admin/DataPeserta')->with(['success' => 'Data berhasil dimasukkan']);
    }
    public function editPeserta($id)
    {
        $user = Auth::user();
        $gambar = peserta_dummy::where('NISN',$id)->first();
        $datasekolah = DB::table('sekolah_dummy')->get();
        $datapeserta = DB::table('peserta_dummy')->where('NISN',$id)->get();
        return view('templateAdmin.page.editPeserta', [
            "user" => $user
            ,'datapeserta' => $datapeserta
            ,'datapeserta' => $datapeserta,
            'datasekolah' => $datasekolah
        ]);

    }
    public function editPesertaFungsi(Request $request, $NISN)
    {
        $request->validate([
            'namapeserta'   => 'required|string',
            'notlp'         => 'required|numeric|min:10',
            'password'      => 'required',
            'sekolah'       => 'required',
            'email'         => 'required|email',
            'tanggalmasuk'  => 'required|date',
            'tanggalkeluar' => 'required|date',
            'foto'          => 'required|image|max:2048'

        ]);
        $gambar = peserta_dummy::where('NISN',$NISN)->first();
        File::delete($gambar->foto);

        $nisn           = $request->input('nisn');
        $foto           = $request->file('foto');
        $extension      = $foto->extension();
        $no_tlp         = $request->input('notlp');
        $password       = $request->input('password');
        $password1      = Hash::make($request->input('password'));
        $namapeserta    = $request->input('namapeserta');
        $sekolah        = $request->input('sekolah');
        $email          = $request->input('email');
        $tanggalmasuk   = $request->input('tanggalmasuk');
        $tanggalkeluar  = $request->input('tanggalkeluar');
        $updated        = Carbon::now()->toDateTimeString();

        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_'.$request->foto->getClientOriginalName();
            $path = $request->file('foto')->storeAs('foto',$fileNameToStore);
            $pathToStore = copy($request->file('foto'), "foto/$fileNameToStore");
        DB::table('peserta_dummy')->where('NISN','=',$nisn)->update([
            "foto"      =>$path,
            'no_tlp'    =>$no_tlp,
            "password"  =>$password,
            "nama"      =>$namapeserta,
            "sekolah"   =>$sekolah,
            "email"     =>$email,
            "tanggal_masuk"=>$tanggalmasuk,
            "tanggal_keluar"=>$tanggalkeluar,
            "updated_at"=>$updated
        ]);
         DB::table('users')->where('NISN','=',$nisn)->update([
            "password"  =>$password1,
            "name"      =>$namapeserta,
            "email"     =>$email
        ]);
        return redirect('/admin/DataPeserta')->with(['success' => 'Data berhasil diubah']);
    }

    public function dataTugas()
    {
        $datatugas = DB::table('t_tugas')->paginate(10);
        $datapeserta = peserta_dummy::all();
        $alldatatugas = DataTugas::all();
        $user = Auth::user();
        return view('templateAdmin.page.dataTugas', [
            'datatugas' => $datatugas,
            "datapeserta" => $datapeserta,
            "alldatatugas" => $alldatatugas,
            "user" => $user
        ]);
    }

    public function detailPeserta($id)
    {
        $user = Auth::user();
        $datapeserta = peserta_dummy::where('NISN',$id)->get();
        return view('templateAdmin.page.detailPeserta', [
            "user" => $user
            ,'datapeserta' => $datapeserta
        ]);
    }
    public function tambahnilai()
    {
        $user = Auth::user();
        return view('templateAdmin.page.tambahnilai', [
            "user" => $user
        ]);
    }
    public function tambah_tugas()
    {
        $user = Auth::user();
        $datapeserta = peserta_dummy::all();
        return view('templateAdmin.page.tambahTugas', [
            "datapeserta" => $datapeserta,
            "user" => $user
        ]);
    }
    public function penilaian($id)
    {
        $user = Auth::user();
        $datapeserta = peserta_dummy::where('NISN',$id)->get();
        return view('templateAdmin.page.penilaian', [
            "user" => $user
            ,'datapeserta' => $datapeserta
        ]);
    }
    public function edit_tugas($id)
    {
        $user = Auth::user();
        $datapeserta = peserta_dummy::all();
        $datatugas = DB::table('t_tugas')->where('id_tugas',$id)->get();
        return view('templateAdmin.page.editTugas', [
            "datapeserta" => $datapeserta,
            "user" => $user
            ,'datatugas' => $datatugas
        ]);

    }
    public function tambah_tugas_fungsi(Request $request)
    {
        $user = Auth::user();
        $nisn = $request->input('nisn');
        $tugas = $request->input('tugas');
        $tanggal_diberikan = $request->input('tanggal_diberikan');
        $status = $request->input('status');
        $data=array('tugas'=>$tugas,"tanggal_diberikan"=>$tanggal_diberikan,"status"=>$status);
        $data=array('NISN'=>$nisn,'tugas'=>$tugas,"tanggal_diberikan"=>$tanggal_diberikan,"status"=>$status);
        DB::table('t_tugas')->insert($data);
       return redirect('/admin/DataTugas');
    }

    public function hapus_data_tugas(Request $request, $id) {
        $datatugas = DataTugas::find($id)->delete();
        return redirect('/admin/DataTugas');
    }

    public function penilaianfungsi(Request $request)
    {
        $user = Auth::user();
        DB::table('penilaian')->where('id_penilaian',$request->id)->update([
        'NISN' => $request->nisn,
        'kreativitas' => $request->kreativitas,
        'ketelitian' => $request->ketelitian,
        'sistematika' => $request->sistematika,
        'inisiatif' => $request->inisiatif,
        'tanggung_jawab' => $request->tanggungjawab,
        'komunikasi' => $request->komunikasi,
        'penyesuaian_diri' => $request->penyesuaiandiri,
        'kerja_sama' => $request->kerjasama,
        'disiplin' => $request->disiplin,
        'kehadiran' => $request->kehadiran
    ]);
       return redirect('/admin/DataPeserta');
    }
    
    public function edit_tugas_fungsi(Request $request)
{
    
    DB::table('t_tugas')->where('id_tugas',$request->id)->update([
        'NISN' => $request->NISNN,
        'tugas' => $request->tugas,
        'tanggal_diberikan' => $request->tanggal_diberikan,
        'status' => $request->status
    ]);

    
        return redirect('/admin/DataTugas');
}

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function tambahPeserta()
    {
        $user = Auth::user();
        $datasekolah = DB::table('sekolah_dummy')->get();
        return view('templateAdmin.page.tambahpeserta', [
            "user" => $user,
            'datasekolah' => $datasekolah
        ]);
    }

    public function sekolah()
    {
        $datasekolah = DB::table('sekolah_dummy')->paginate(10);
        $user = Auth::user();
        return view('templateAdmin.page.sekolah', [
            'datasekolah' => $datasekolah,
            "user" => $user
        ]);
    }

    public function tambah_sekolah()
    {
        $user = Auth::user();
        return view('templateAdmin.page.tambahSekolah', [
            "user" => $user
        ]);
    }

    public function edit_sekolah($id)
    {
        $user = Auth::user();
        $datasekolah = DB::table('sekolah_dummy')->where('id_sekolah',$id)->get();
        return view('templateAdmin.page.editSekolah', [
            "user" => $user
            ,'datasekolah' => $datasekolah
        ]);

    }

    public function tambah_sekolah_fungsi(Request $request)
    {
        // dd($request);
        $user               = Auth::user();
        $NPSN               = $request->input('npsn');
        $nama_sekolah       = $request->input('nama_sekolah');
        $alamat             = $request->input('alamat');
        $nama_pembimbing    = $request->input('nama_pembimbing');
        $no_tlp             = $request->input('no_telp');
        $foto               = $request->file('foto');
        $extension          = $foto->extension();
        $email              = $request->input('email');
        $password           = $request->input('password');
        $password1          = Hash::make($request->input('password'));
        $admin              = $request->input('admin');

        
        $fileExt = $request->file('foto')->getClientOriginalExtension();
        $fileNameToStore = time().'_sklh_'.$request->foto->getClientOriginalName();
        $pathToStore = $request->file('foto')->move('foto',$fileNameToStore);

        $data=array('id_sekolah'=>$NPSN,
            "nama_sekolah"=>$nama_sekolah,
            "alamat_sekolah"=>$alamat,
            "nama_pembimbing"=>$nama_pembimbing,
            'no_tlp'=>$no_tlp,
            "foto_sekolah"=>$pathToStore,
            "email"=>$email,
            "password"=>$password);

        $data1=array('id_sekolah'=>$NPSN,
            "password"=>$password1,
            "name"=>$nama_sekolah,
            "email"=>$email,
            "admin"=>$admin);
        DB::table('users')->insert([$data1]);
        
        DB::table('sekolah_dummy')->insert([$data]);


       return redirect('/admin/sekolah');
    }
     public function hapus_data_sekolah(Request $request, $id_sekolah) {

        sekolah_dummy::where('id_sekolah',$id_sekolah)->delete();
        User::where('id_sekolah',$id_sekolah)->delete();
        
        return redirect('/admin/sekolah')->with(['success' => 'Data berhasil dihapus']);
    }

     public function edit_sekolah_fungsi(Request $request)
    {
        // dd($request);
        $request->validate([
            'foto.*' => 'mimes:jpg,jpeg,png,jpg,gif|max:2048'

        ]);

        $user               = Auth::user();
        $NPSN               = $request->input('npsn');
        $nama_sekolah       = $request->input('nama_sekolah');
        $alamat             = $request->input('alamat');
        $nama_pembimbing    = $request->input('nama_pembimbing');
        $no_tlp             = $request->input('no_telp');
        $foto               = $request->file('foto');
        $extension          = $foto->extension();
        $email              = $request->input('email');
        $password           = $request->input('password');
        $password1          = Hash::make($request->input('password'));
        $admin              = $request->input('admin');

        // $gambar = sekolah_dummy::where('id_sekolah',$NPSN)->first();
        // File::delete($gambar->foto_sekolah);
        
        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_sklh_'.$request->foto->getClientOriginalName();
            $path = $request->file('foto')->storeAs('foto',$fileNameToStore);
            $pathToStore = copy($request->file('foto'), "foto/$fileNameToStore");

        DB::table('sekolah_dummy')->where('id_sekolah','=',$NPSN)->update([
            "nama_sekolah"=>$nama_sekolah,
            "alamat_sekolah"=>$alamat,
            "nama_pembimbing"=>$nama_pembimbing,
            "no_tlp"=>$no_tlp,
            "foto_sekolah"=>$path,
            "email"=>$email,
            "password"=>$password
        ]);

        DB::table('users')->where('id_sekolah','=',$NPSN)->update([
            "password"  =>$password1,
            "name"      =>$nama_sekolah,
            "email"     =>$email
        ]);

    
            return redirect('/admin/sekolah');
    }
    public function edit_profile_admin() {
        $user = Auth::user();
        return view('templateAdmin.page.editProfile',[
            "user" => $user
        ]);
    }
    // public function pengiriman()
    // {
    //     $jasapengiriman = JasaPengiriman::all();
    //     $user = Auth::user();
    //     return view('templateUser.page.jasaPengiriman', [
    //         "jasapengiriman" => $jasapengiriman,
    //         "user" => $user
    //     ]);
    // }

    // public function tambah_jasa_pengiriman_controller(Request $request)
    // {
    //     $penjual = Auth::user()->id;
    //     $request->validate([
    //         'nama' => 'string|max:50',
    //         'lama_pengiriman' => 'integer',
    //         'harga_per_kilo' => 'integer'
    //     ]);

    //     $input = $request->all();

    //     $status = JasaPengiriman::create($input);

    //     return ($status) ? redirect(route('adminPengiriman')) : die($status);
    // }

    // public function tambah_barang()
    // {
    //     $kategori = KategoriBarang::all();
    //     $user = Auth::user();
    //     return view('templateUser.page.tambahBarang', [
    //         "kategori" => $kategori,
    //         "user" => $user
    //     ]);
    // }

    // public function generate_id_barang() {
    //     $asd = "";
    //     do {
    //         $randomInt = rand(1, 25);
    //         $randomString = str::random($randomInt);
    //         $prefix = "INV";
    //         $asd = $prefix . $randomString;
    //         $id = Barang::where('id_barang', $asd)->get();
    //     } while (!$id->isEmpty());
    //     return $asd;
    // }

    // public function tambah_barang_controller(Request $request)
    // {
    //     // id barang
    //     $idBarang = $this->generate_id_barang();

    //     // penjual
    //     $penjual = Auth::user()->id;

    //     // validasi
    //     $request->validate([
    //         'nama_barang' => 'string|max:200',
    //         'harga_satuan' => 'integer',
    //         'berat' => 'integer|max:10',
    //         'stock' => 'integer',
    //         'pembelian_minimum' => 'required|min:1',
    //         'foto.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
    //     ]);

        // request semua data
    //     $input = $request->all();

    //     // set id penjual
    //     $input['id_penjual'] = $penjual;
    //     $input['id_barang'] = $idBarang;

    //     // get foto[] dari request
    //     $foto['foto'] = $request->file('foto');

    //     // hapus request foto
    //     unset($input['foto']);

    //     // create barang sesuai request
    //     $status = Barang::create($input);

    //     // create foto barang
    //     for($i = 0; $i < count($foto['foto']); $i++) {
    //         $fotoBarang = new FotoBarangHelper($foto['foto'][$i], $idBarang);
    //         $fotoBarang->store();
    //     }

    //     // redirect ketika success
    //     return ($status) ? 
    //         redirect('/admin/barang') :
    //         redirect('/admin/barang/tambahBarang');
    // }

    // public function detail_barang_controller($id)
    // {
    //     $barang = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')->find($id);
    //     $fotoBarang = FotoBarang::where('id_barang', $id)->get();
    //     $user = Auth::user();
    //     return view('templateUser.page.detailBarang', [
    //         "barang" => $barang,
    //         "fotoBarang" => $fotoBarang,
    //         "user" => $user
    //     ]);
    // }

    // public function edit_barang($id)
    // {
    //     $barang = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')
    //                         ->find($id);
    //     $kategori = KategoriBarang::all();
    //     $user = Auth::user();
    //     $fotoBarang = FotoBarang::where('id_barang', $id)->get();
    //     return view('templateUser.page.tambahBarang', [
    //         "barang" => $barang,
    //         "kategori" => $kategori,
    //         "user" => $user,
    //         "fotoBarang" => $fotoBarang
    //     ]);
    // }

    // public function edit_barang_controller(Request $request, $id)
    // {
    //     $penjual = Auth::user()->id;
    //     $request->validate([
    //         'nama_barang' => 'string|max:200',
    //         'harga_satuan' => 'integer',
    //         'berat' => 'integer|max:10',
    //         'stock' => 'integer',
    //         'pembelian_minimum' => 'required|min:1',
    //         'foto.*' => 'mimes:jpeg,png,jpg,gif|max:2048'
    //     ]);

    //     $input = $request->all();

    //     if($request->hasfile('foto')) {
    //         $foto['foto'] = $request->file('foto');
    //         $fotoBarangHelper = new FotoBarangHelper($foto['foto'], $id);
    //         $fotoBarangHelper->update();
    //     }

    //     unset($input['foto']);

    //     $input['id_penjual'] = $penjual;

    //     $status = Barang::find($id)->update($input);

    //     return ($status) ? 
    //         redirect('/admin/barang') :
    //         redirect('/admin/barang/tambahBarang');
    // }

    // public function hapus_barang_controller($id)
    // {
    //     // 1. Hapus foto dari public folder
    //     File::deleteDirectory(public_path() . "/foto_barang/$id");

    //     // 2. Hapus foto
    //     FotoBarang::where('id_barang', $id)->delete();

    //     // 3. Hapus Barang
    //     $status = Barang::find($id)->delete();

    //     $user = Auth::user();

    //     $barang = Barang::where('id_penjual', $user->id)
    //                     ->join('kategori_barang', 'barang.kategori_barang', '=', 'kategori_barang.id_kategori')
    //                     ->get();

    //     return view('templateUser.page.tableBarang', [
    //         "barang" => $barang,
    //         "user" => $user
    //     ]);
    // }


    // public function tambah_kategori_barang(Request $request) {

    //     // 1. Validasi input
    //     $request->validate([
    //         'nama_kategori' => 'required|string|max:50'
    //     ]);

    //     // 2. Ambil data semua Request
    //     $input = $request->all();

    //     // 3. Tambahkan data ke database
    //     $status = KategoriBarang::create($input);

    //     // 4. Redirect page
    //     return ($status) ? 
    //         redirect('/admin/kategori') : die($status);
    // }

    // public function hapus_kategori_barang($id) {
    //     $status = KategoriBarang::find($id)->delete();
    //     return redirect('/admin/kategori');
    // }

    // public function edit_kategori_barang(Request $request, $id) {
    //     $input = $request->all();
    //     KategoriBarang::find($id)->update($input);
    //     return redirect('/admin/kategori');
    // }

    
}
