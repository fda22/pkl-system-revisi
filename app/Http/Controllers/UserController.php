<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\DataTugas;
use Carbon\Carbon;
use App\User;
use App\peserta_dummy;
use App\kehadiran_dummy;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use File;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function indexUser()
    {
       $datatugas = DB::table('t_tugas')->get();

       // Contoh akses fungsi belongs to ke fungsi user dan mengambil data emailnya
        $kehadiran = DB::table('kehadiran_dummy')->get();
        // foreach($kehadiran as $item) {
        //     var_dump($item->peserta_dummy->NISN);
        // }
            $hadir = kehadiran_dummy::where('kehadiran', '=', 'Hadir' )->get();
            $izin = kehadiran_dummy::where('kehadiran', '=', 'Izin' )->get();
            $sakit = kehadiran_dummy::where('kehadiran', '=', 'Sakit' )->get();
            $alfa = kehadiran_dummy::where('kehadiran', '=', 'Alfa' )->get();
            $hari = Carbon::now()->format('Y-m-d');
            $user = Auth::user();
            $totalHadir = count($hadir);
            $totalIzin = count($izin);
            $totalSakit = count($sakit);
            $totalAlfa = count($alfa);
            $kehadiran_sekarang = kehadiran_dummy::where('tanggal_kehadiran', $hari)->exists();

            return view('templateUser.page.home', [
            "hari" => $hari,
            "kehadiran" => $kehadiran,
            "datatugas" => $datatugas,
            "totalHadir" => $totalHadir,
            "totalIzin" => $totalIzin,
            "totalSakit" => $totalSakit,
            "totalAlfa" => $totalAlfa,
            "kehadiran_sekarang" => $kehadiran_sekarang,
            "user" => $user
            ]);
    }

    public function tambah_kehadiran(Request $request)
    {   
       $user = Auth::user();


       if($request["keterangan"] == null || $request["keterangan"] == "") {
            $request["keterangan"] = "-";
        }
        $mytime = Carbon::now()->toDateTimeString();
        $kehadiran = $request->input('kehadiran');
        $keterangan = $request->input('keterangan');
        $NISN = $request->input('NISN');
        $data=array('kehadiran'=>$kehadiran,'keterangan'=>$keterangan, 'NISN'=>$NISN, 'tanggal_kehadiran'=> $mytime);
        DB::table('kehadiran_dummy')->insert($data);
       return redirect('/user/Home');
    }

    public function tugas() 
    {
        $datatugas = DataTugas::all();

        $user = Auth::user();
        return view('templateUser.page.tableTugas', [
            "datatugas" => $datatugas,
            "user" => $user
        ]);
    }

    public function update_data_tugas(Request $request, $id)
    {
       $user = Auth::user();
        DB::table('t_tugas')->where('id_tugas',$request->id)->update([
        'status' => $request->update_data_tugas
         ]);
       return redirect('/user/tugas');
    }
    public function edit_profile() {
        $user = Auth::user();
        return view('templateUser.page.editProfile',[
            "user" => $user
        ]);
    }

    public function editPesertaFungsi(Request $request)
    {
        $request->validate([
            'foto'          => 'required|image|max:2048'
        ]);

        // ]);
        // $gambar = peserta_dummy::where('NISN',$NISN)->first();
        // File::delete($gambar->foto);

        $nisn           = $request->input('nisn');
        $foto           = $request->file('foto');
        $extension      = $foto->extension();
        $no_tlp         = $request->input('notlp');
        $password       = $request->input('password');
        $password1      = Hash::make($request->input('password'));
        $namapeserta    = $request->input('namapeserta');
        $sekolah        = $request->input('sekolah');
        $email          = $request->input('email');
        $updated        = Carbon::now()->toDateTimeString();

        $fileExt = $request->file('foto')->getClientOriginalExtension();
            $fileNameToStore = time().'_'.$request->foto->getClientOriginalName();
            $path = $request->file('foto')->storeAs('foto',$fileNameToStore);
            $pathToStore = copy($request->file('foto'), "foto/$fileNameToStore");
        DB::table('peserta_dummy')->where('NISN','=',$nisn)->update([
            "foto"      =>$path,
            'no_tlp'    =>$no_tlp,
            "password"  =>$password,
            "nama"      =>$namapeserta,
            "sekolah"   =>$sekolah,
            "email"     =>$email,
            "updated_at"=>$updated
        ]);
         DB::table('users')->where('NISN','=',$nisn)->update([
            "password"  =>$password1,
            "name"      =>$namapeserta,
            "email"     =>$email
        ]);
        return redirect('/user/editProfile')->with(['success' => 'Data berhasil diubah']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
 
}
