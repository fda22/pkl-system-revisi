<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kehadiran_dummy extends Model
{
    public $table = 'kehadiran_dummy';

	protected $fillable = ['NISN','kehadiran', 'keterangan'];
	protected $guarded = ['id_kehadiran'];

	protected $primarykey = ['id_kehadiran'];  
	
	public $timestamps = false;

	function kehadiran_dummy()
	{
		return $this->belongsTo(peserta_dummy::class, 'NISN', 'NISN');
	}
}
