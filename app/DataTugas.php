<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataTugas extends Model
{
    public $table = 't_tugas';

    protected $fillable = ['NISN', 'tugas', 'tanggal_diberikan', 'status'];
    protected $guarded = ['id_tugas'];

    protected $primaryKey = "id_tugas";

    function datatugas(){
		return $this->belongsTo(peserta_dummy::class,'NISN', 'NISN');
	}
}
