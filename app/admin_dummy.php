<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin_dummy extends Model
{
    public $table = 'admin_dummy';

	protected $fillable = ['email', 'password', 'nama_perusahaan', 'nama_admin', 'logo_perusahaan', 'foto_admin', 'alamat_perusahaan', 'notelp_perushaan'];
	protected $guarded = ['admin_dummy'];

	protected $primarykey = ['id_admin']; 

	function admin_dummy(){
		return $this->belongsTo('App\User','id_admin');
	}   
}
