<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penilaian extends Model
{
    public $table = 'penilaian';

    protected $fillable = ['NISN','kreativitas', 'ketelitian', 'sistematika', 'inisiatif', 'tanggung_jawab', 'komunikasi', 'penyesuaian_diri', 'kerja_sama', 'disiplin', 'kehadiran'];
    protected $guarded = ['id_penilaian'];

    protected $primaryKey = "id_penilaian";

    function peserta_dummy(){
		return $this->belongsTo(peserta_dummy::class,'NISN', 'NISN');
	}
}
