<!DOCTYPE html>
<html>
	<head>
		<title>Data Peserta</title>
	</head>
	<body>
		<div class="card">
			<!-- /.card-header -->
			<div class="card-body">
				<div class="row">
					<div class="card-body table-responsive p-0">
						<table id="example2" class="table table-hover">
							<thead>
								<tr role="row">
									<th>Nama</th>
									<th>Sekolah</th>
									<th>No telepon</th>
									<th>Email</th>
									<th>Tanggal masuk</th>
									<th>Tanggal keluar</th>
								</tr>
							</thead>
							<tbody>
								
								@foreach($datapeserta as $DP)
								<tr role="row">
									<td>{{ $DP->nama}} </td>
									<td>{{ $DP->sekolah }}</td>
									<td>{{ $DP->no_tlp}}</td>
									<td>{{ $DP->email}}</td>
									<td>{{ $DP->tanggal_masuk }}</td>
									<td>{{ $DP->tanggal_keluar }}</td>
								</tr>
							</tbody>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>