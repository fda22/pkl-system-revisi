<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Pkl-System</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="hold-transition login-page" style="background:url('images/backgrond.png'); width: 100%;">
            @yield('content')
</body>
</html>
<script>
    function toggler(e) {
        if( e.innerHTML == '<img src="images/show.png">' ) {
            e.innerHTML = '<img src="images/hide.png">'
            document.getElementById('password').type="text";
        } else {
            e.innerHTML = '<img src="images/show.png">'
            document.getElementById('password').type="password";
        }
}
</script>
