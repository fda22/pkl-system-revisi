@extends('layouts.app')

@section('content')
<div class="login-box" style="font-family: Segoe UI" >
    <div class="card"  style="
    background-color: transparent;
    border: 0px solid rgba(0,0,0,.125);
    width: 360px;
    top: 50%;
    left: 50%;
    margin-top: 100px;
    margin-left: -190px;">
               <img src="images/logo.png" style="display: block; margin: 0 auto; text-align: center; width: 135px; height: 135px;">
        <div class="card-body login-card-body">
            <!-- <div class="card-header">{{ __('Login') }}</div> --> 
            <form method="POST" action="{{ route('login') }}">
                @csrf
                 <label for="email" class="col-md-4" style="max-width: 100%; font-size: 16px; margin-left: -15px;">{{ __('E-Mail Address') }}</label>

                    <div class="input-group mb-3">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" style="background-color: #C9C9C9; color: #000; border-radius: 4px; border-style: none;" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                 <label for="password" class="col-md-4" style="max-width: 100%; font-size: 16px; margin-left: -15px;">{{ __('Password') }}</label>
                    <div class="input-group mb-3">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" style="background-color: #C9C9C9; color: #000; border-style: none; border-radius: 4px 0 0 4px " name="password" required autocomplete="current-password">
                        <button onclick="toggler(this)" type="button" style="background-color:  #C9C9C9 ; border-style: none; border-radius: 0 4px 4px 0 "> <img src="images/show.png"></button>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                       <!--  <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="width: 100px;height: 40px; border-radius: 20px; background-color: #FBAC2D; border: 0px solid rgba(0,0,0,.125);">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a> -->
                                @endif
                            </div>
                        </div>
                    </form>
        </div>
    </div>
</div>
@endsection
