<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Arkamaya</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
    .user-panel img {
      width: 2.1rem;
      height: auto;
      margin-top: 12px;
    }
    [class*=sidebar-light] .user-panel {
    border-bottom: none;
    margin-bottom: -20px;
    }
    .pb-3, .py-3 {
     padding-bottom: 0rem!important; 
    }
    .sidebar-light-warning .nav-sidebar>.nav-item>.nav-link.active {
      box-shadow: none;
    }
    .sidebar-light-warning .nav-sidebar>.nav-item>.nav-link.active {
      color: #ffc107;
      background-color: transparent;
    }
    .sidebar-light-warning .nav-sidebar>.nav-item:hover>.nav-link {
      color: #007bff;
      background-color: #f4f4f5;
    }
    .navbar-light .navbar-nav .nav-item:hover>.nav-link-logout {
      color: #dc3545;
    }
    </style>
  </head>
  <body class="hold-transition sidebar-mini">