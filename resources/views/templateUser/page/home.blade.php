@include('templateUser.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item" style="border-radius: 5px" >
        <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
          <i class="nav-icon fas fa-sign-out-alt"></i>Logout
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-warning">
    <!-- Brand Logo -->
    <a href="{{ url('./') }}" class="brand-link">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('images/profile_user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('./') }}" class="" style="font-weight: bold;">{{ $user->name }}</a>
          <p>
            <a href="{{ url('./') }}" class="" style="font-size: 12px; color: #6c757d;">{{ $user->email }}</a>
          </p>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('userTugas') }}" class="nav-link">
              <i class="fas fa-tasks"></i>
              <p class="text">Tugas</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('userEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
              </ol>
              </div><!-- /.col -->
              </div><!-- /.row -->
              </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-info elevation-1"><i class="far fa-hand-paper"></i></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Hadir</span>
                        <span class="info-box-number">{{ $totalHadir }}
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-walking"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Izin</span>
                        <span class="info-box-number">{{ $totalIzin }}
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <!-- fix for small devices only -->
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3" >
                      <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-procedures" style="color: white"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Sakit</span>
                        <span class="info-box-number">{{ $totalSakit }}
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-exclamation"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Tanpa Keterangan</span>
                        <span class="info-box-number">{{ $totalAlfa }}
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                </div>
                <p id="daytime" style="color: grey"></p>
               @if (!$kehadiran_sekarang)
                  <div class="card-deck" >
                    <div class="card text-white bg-info mb-3 text-center" style="max-width: 11rem; height: 9rem">
                      <div class="card-header">Jam Masuk</div>
                      <div class="card-body" style="padding: 7px;">
                        <h4 id="datetimeMasuk" name="hadir">-</h4>
                        <button type="submit" onclick="clearInterval(myVar); setIntervalPulang(); removeBtnMasuk();" class="btn btn-info" id="masuk" href="Home">Cek In</button>
                      </div>
                    </div>
                    <div class="card text-white bg-warning mb-3 text-center" style="max-width: 11rem; height: 9rem;">
                      <div class="card-header" style="color: white">Jam Pulang</div>
                      <div class="card-body" style="padding: 7px;">
                        <h4 style="color: white" id="datetimePulang">-</h4>
                        <button onclick="removeIntervalPulang(); jumlahJam(); removeBtnPulang();" class="btn btn-warning" id="pulang">Cek Out</button>
                      </div>
                    </div>
                    <div class="card text-white bg-success mb-3 text-center" style="max-width: 11rem; height: 9rem">
                      <div class="card-header">Total Jam</div>
                      <div class="card-body" style="padding: 7px;">
                        <h1 id="totalJam">-</h1>
                      </div>
                    </div>
                  </div>
                  <input class="form-check-input" type="hidden" name="kehadiran" value="Izin">
                  <input class="form-check-input" type="hidden" name="kehadiran" value="Sakit">
                  <input class="form-check-input" type="hidden" name="kehadiran" value="Alfa">
                  <input type="hidden" class="form-control" rows="5" placeholder="Enter ..." style="margin-bottom: 10px; height: 115px;" name="keterangan">
                  </form>
                  <!-- Info boxes --> 
                  <form action="{{ route('tambah_kehadiran')}}" method="POST">
                  <div class="row">
                    <div class="col-md-6" id="deck">
                      @csrf
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">
                          Status
                          </h3>
                        </div>
                        <!-- /.card-header -->
                        <input type="hidden" name="NISN" value="{{ $user->NISN}}">
                        <div class="card-body" style="display: flex;">
                          <div class="form-check" style="margin-right: 10px;">
                            <input class="form-check-input" type="radio" name="kehadiran" value="Izin">
                            <label class="form-check-label">Izin</label>
                          </div>
                          <div class="form-check" style="margin-right: 10px;">
                            <input class="form-check-input" type="radio" name="kehadiran" value="Sakit">
                            <label class="form-check-label">Sakit</label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="kehadiran" value="Alfa">
                            <label class="form-check-label">Tanpa Keterangan</label>
                          </div>
                        </div>
                        <div class="card-body" style="margin-top: -20px">
                          <div class="form-group">
                            
                            <label>Keterangan</label>
                            
                            <input type="textarea" class="form-control" rows="5" placeholder="Enter ..." style="margin-bottom: 10px; height: 115px;" name="keterangan">
                            <button type="submit" class="btn btn-block btn-outline-warning" onclick="removeCardKehadiran();">Kirim</button>
                          </div>
                        </div>
                      </div>
                      <!-- /.card-body -->
                      
                      
               
                      <!-- /.card -->
                    </div>
                    @endif
                  <!-- /.col (left) -->
                  <div class="col-md">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">List Tugas</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body table-responsive p-0" style="height: 295px;">
                        <table class="table table-head-fixed">
                          <tr>
                            <th style="width: 10px">No</th>
                            <th class="text-center">Task</th>
                            <th class="text-center">Deadline</th>
                            <th class="text-center">Status</th>
                          </tr>
                          <?php $i = 1; ?>
                          <tr>
                            <td class="text-center">{{ $i }}</td>
                            {{-- <td class="text-center"></td> --}}
                            <td class="text-center">{{ $user->peserta_dummy->datatugas->tugas }}</td>
                            <td class="text-center">{{ $user->peserta_dummy->datatugas->tanggal_diberikan }}</td>
                            <td class="text-center">{{ $user->peserta_dummy->datatugas->status }}</td>
                          </tr>
                          <?php $i++; ?>
                        </table>
                      </div>
                      <!-- /.card-body -->
                    </div>
                  </div>
                  <!-- /.col (right) -->
                </div>
                <!-- /.row -->
                <div class="card card-default">
                  <div class="card-header">
                    <h3 class="card-title">Dialy Report</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body" style="display: block;">
                    <div class="row">
                      <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                      <button type="submit" class="btn btn-outline-warning" style="margin-top: 10px;">Submit</button>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                </div>
                </div><!--/. container-fluid -->
              </section>
              <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
              <!-- To the right -->
              <div class="float-right d-sm-none d-md-block">
                Ini Project
              </div>
              <!-- Default to the left -->
              <strong>Copyright &copy; 2019 <a href="https://www.arkamaya.co.id/">Arkamaya</a>.</strong> All rights reserved.
            </footer>
          </div>
          <script type="text/javascript">
          n =  new Date();
          y = n.getFullYear();
          d = n.getDate();
          weekdays = new Array(7);
          weekdays[0] = "Minggu";
          weekdays[1] = "Senin";
          weekdays[2] = "Selasa";
          weekdays[3] = "Rabu";
          weekdays[4] = "Kamis";
          weekdays[5] = "Jumat";
          weekdays[6] = "Sabtu";
          r = weekdays[n.getDay()];
          bulan = new Array(13);
          bulan[1] = "Januari";
          bulan[2] = "Febuari";
          bulan[3] = "Maret";
          bulan[4] = "April";
          bulan[5] = "Mei";
          bulan[6] = "Juni";
          bulan[7] = "Juli";
          bulan[8] = "Agustus";
          bulan[9] = "September";
          bulan[10] = "Oktober";
          bulan[11] = "November";
          bulan[12] = "Desember";
          m = bulan[n.getMonth()+1];
          document.getElementById("daytime").innerHTML ="Hari "  + r + ", "+ d + " " + m + " " + y;
          var myVar=setInterval(function(){myTimer()},1);
          function myTimer() {
          var d = new Date();
          var hour=d.getHours();
          var minute=d.getMinutes();
          var second=d.getSeconds();
          var millisecond=d.getMilliseconds();
          if(hour <10 ){hour='0'+hour;}
          if(minute <10 ) {minute='0' + minute; }
          if(second<10){second='0' + second;}
          var z = document.getElementById("datetimeMasuk").innerHTML =hour+":"+minute+":"+second;
          }
          // var myVarPulang=setInterval(function(){myTimerPulang()},1000);
          var pulang;
          var setIntervalPulang = () => pulang = setInterval(function(){myTimerPulang()},1);
          var removeIntervalPulang = () => clearInterval(pulang);
          function myTimerPulang() {
          var d = new Date();
          var hour=d.getHours();
          var minute=d.getMinutes();
          var second=d.getSeconds();
          if(hour <10 ){hour='0'+hour;}
          if(minute <10 ) {minute='0' + minute; }
          if(second<10){second='0' + second;}
          var x =document.getElementById("datetimePulang").innerHTML = hour+":"+minute+":"+second;
          // var z =document.getElementById("datetimeMasuk");
          // document.getElementById("totalJam").innerHTML = parseInt(x) - parseInt(z);
          }
          
          var jumlahJam = () => {
          var jamPulang =document.getElementById("datetimePulang").innerHTML.split(":")[0];
          var jamMasuk =document.getElementById("datetimeMasuk").innerHTML.split(":")[0];
          
          document.getElementById("totalJam").innerHTML = parseInt(jamPulang) - parseInt(jamMasuk);
          }
          // document.getElementById("datetimePulang") - document.getElementById("datetimeMasuk");
          document.getElementById('pulang').style.display = 'none';
          function removeBtnMasuk() {
          var elem = document.getElementById('masuk');
          elem.parentNode.removeChild(elem);
          document.getElementById('pulang').style.display = '';
          }
          function removeBtnPulang() {
          var x = document.getElementById('pulang').style.visibility = "hidden";
          x.parentNode.removeChild(x);
          }
          function removeCardKehadiran() {
            var x = document.getElementById('deck').style.visibility = "hidden";
            x.parentNode.removeChild(x);
          }
          
          </script>
          @include('templateUser.footer')