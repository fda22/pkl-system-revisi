@include('templateUser.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-warning">
    <!-- Brand Logo -->
    <a href="{{ url('./') }}" class="brand-link">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('images/profile_user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('./') }}" class="" style="font-weight: bold;">{{ $user->name }}</a>
          <p>
          <a href="{{ url('./') }}" class="" style="font-size: 12px; color: #6c757d;">{{ $user->email }}</a>
          </p>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('./') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="fas fa-tasks"></i>
              <p class="text">Tugas</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('userEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Tugas</li>
              </ol>
              </div><!-- /.col -->
              </div><!-- /.row -->
              </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- Info boxes -->
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <div class="card card-warning">
                      <div class="card-header">
                        <h3 class="card-title" style="color: white">Tugas</h3>
                      </div>
                      <!-- /.card-header -->
                      @if ($datatugas)
                      <div class="card-body p-0">
                        <table class="table table-condensed">
                          <tr>
                            <th style="width: 10px">No</th>
                            <th class="text-center">Task</th>
                            <th class="text-center">Deadline</th>
                            <th class="text-center">Status</th>
                          </tr>
                          <?php $i = 1; ?>
                          <tr>
                            <td class="text-center"></td>
                            {{-- <td class="text-center"></td> --}}
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center">
                            </td>
                          </tr>

                          <?php $i++; ?>
                          
                        </table>
                      </div>
                      @elseif(!$datatugas)
                      <div class="card-body p-0">
                        <table class="table table-condensed">
                          <tr>
                            <th style="width: 10px">No</th>
                            <th class="text-center">Task</th>
                            <th class="text-center">Deadline</th>
                            <th class="text-center">Status</th>
                          </tr>
                          <?php $i = 1; ?>
                          <tr>
                            <td class="text-center">{{ $i }}</td>
                            {{-- <td class="text-center"></td> --}}
                            <td class="text-center">{{ $user->peserta_dummy->datatugas->tugas }}</td>
                            <td class="text-center">{{ $user->peserta_dummy->datatugas->tanggal_diberikan }}</td>
                            <td class="text-center">{{ $user->peserta_dummy->datatugas->status }}</td>
                            <td class="text-center">
                              <div class="btn-group">
                                <button type="button" class="btn btn-warning">Action</button>
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                </button> 
                                @foreach ($datatugas as $data)
                                <div class="dropdown-menu" role="menu" style="margin-left: -75px">
                                  <form action="{{ url("user/tugas$data->id_tugas") }}" method="POST">
                                    @csrf
                                    <button type="submit" class="dropdown-item" name="update_data_tugas" value="Di Kerjakan">Di Kerjakan</button>
                                    <div class="dropdown-divider"></div>
                                    @csrf
                                    <button type="submit" class="dropdown-item" name="update_data_tugas" value="Belum Di Kerjakan">Belum Di Kerjakan</button>
                                  </form>
                                  @endforeach
                                </div>
                              </div>
                            </td>
                          </tr>

                          <?php $i++; ?>
                          
                        </table>
                      </div>
                      @endif
                      <!-- /.card-body -->
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
                </div><!--/. container-fluid -->
              </section>
              <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
              <!-- To the right -->
              <div class="float-right d-sm-none d-md-block">
                Anything you want
              </div>
              <!-- Default to the left -->
              <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
            </footer>
          </div>
          <!-- ./wrapper -->
          @include('templateUser.footer')