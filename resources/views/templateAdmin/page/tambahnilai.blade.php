@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
                <a href="{{ route('dataPesertaAdmin') }}" class="nav-link active">
                  <i class="fas fa-portrait" style="color: black;"></i>
                  <p style="color: black;">Data peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dataTugas') }}" class="nav-link">
                  <i class="fas fa-tasks" style="color: black;"></i>
                  <p style="color: black;">Tugas</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolah') }}" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Sekolah</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Penilaian</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Peserta</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('adminHome')}}">Home</a></li>
              <li class="breadcrumb-item active">peserta</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row" style="margin-bottom: 10px; margin-left: 1px">
          <div>
          <button type="button" class="btn btn-block btn-primary" style="" >
            <a style="color: white"  href="{{ route('dataPesertaAdmin')}}">
            <i class="fas fa-arrow-left" style="margin-right: 10px;color: white;"></i>Kembali</button>
            </a>
          </div>
            <div style="margin-left: 84.5%">
          <button type="button" class="btn btn-block btn-primary" style="">
            <i class="fas fa-plus" style="margin-right: 10px"></i>Tambah nilai</button>
          </div>
        </div>
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-3">
                  <h5>NISN</h5>
                    <input type="text" class="form-control" placeholder="" disabled>
                  </div>
                <div class="col-3" style="margin-left: 10px;">
                  <h5>Inisiatif</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="col-3" style="margin-left: 10px">
                  <h5>Kerja sama</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
              </div>
              <div class="row" style="margin-top: 10px;">
                  <div class="col-3">
                <h5>Kreativitas</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                <div class="col-3" style="margin-left: 10px;">
                  <h5>Tanggung jawab</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="col-3" style="margin-left: 10px">
                  <h5>Disiplin</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
              </div>
                  <div class="row" style="margin-top: 10px;">
                  <div class="col-3">
                <h5>Ketelitian</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                <div class="col-3" style="margin-left: 10px;">
                  <h5>Komunikasi</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="col-3" style="margin-left: 10px">
                  <h5>Kehadiran</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
              </div>
              <div class="row" style="margin-top: 10px;">
                  <div class="col-3">
                <h5>Sistematika</h5>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                <div class="col-3" style="margin-left: 10px;">
                  <h5>Penyesuaian diri</h5>
                    <input type="text" class="form-control" placeholder="">
                </div>
              </div>                    
          </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
@include('templateAdmin.footer')
