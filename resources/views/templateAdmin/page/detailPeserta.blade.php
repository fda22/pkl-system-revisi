@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
                <a href="{{ route('dataPesertaAdmin') }}" class="nav-link active">
                  <i class="fas fa-portrait" style="color: black;"></i>
                  <p style="color: black;">Data peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dataTugas') }}" class="nav-link">
                  <i class="fas fa-tasks" style="color: black;"></i>
                  <p style="color: black;">Tugas</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolah') }}" class="nav-link">
              <i class="fas fa-school"></i>
              <p>Sekolah</p>
            </a>
          </li>
           <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Detail peserta</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('adminHome')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('dataPesertaAdmin')}}">Data peserta</a></li>
              <li class="breadcrumb-item active">Detail peserta</li>
            </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">
            <div class="container-fluid">
              <div class="row" style="margin-bottom: 10px;">
                <div class="col-3">
                  <a href="{{ route('dataPesertaAdmin')}}" style="color: white">
                  <button type="button" class="btn btn-block btn-primary" style="">
                    <i class="fas fa-arrow-left" style="margin-right: 10px;color: white;"></i>Kembali</button>
                  </a>
                </div>
              </div>
              <div class="card">
                @foreach($datapeserta as $DP)
                <!-- /.card-header -->
                <div class="card-body">
                    <img style="width: 150px;height: 175px;
                                object-fit: cover;
                                display: block;
                                margin-left: auto;
                                margin-right: auto;"
                                src="{{URL::to($DP->foto)}}">
                <div style="padding-bottom: 3%">
                <h5 style="width: 50%;text-align: center;float: right;">Nilai</h5>  
                <h5 style="width: 50%;text-align: center;float: left;">Data diri</h5>
                    </div>
                    <div class="row" style="width: 50%;float: left;padding-left:14%;">
                      <div class="">
                    <h6>NISN</h6>
                    <h6>Password</h6>
                    <h6>Nama peserta</h6>
                    <h6>Sekolah</h6>
                    <h6>No. telepon</h6>
                    <h6>Email</h6>
                    <h6>Tanggal masuk</h6>
                    <h6>Tanggal keluar</h6>
                    </div>
                    <div class="" style="margin-left: 15px">
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                    </div>
                    <div style="margin-left: 20px">
                      <h6>{{$DP->NISN}}</h6>
                      <input id="password" type="password" name="" value="{{$DP->password}}" style="border: none;background-color: white;" disabled=""><button onclick="toggler(this)" type="button" style="background-color:  white ; border-style: none;"><i class="fas fa-eye"></i></button>
                      <h6>{{$DP->nama}}</h6>
                      <h6>{{$DP->sekolah}}</h6>
                      <h6>{{$DP->no_tlp}}</h6>
                      <h6>{{$DP->email}}</h6>
                      <h6>{{$DP->tanggal_masuk}}</h6>
                      <h6>{{$DP->tanggal_keluar}}</h6>
                    </div>
                    </div>
                    <div class="row" style="width: 50%;float: right;padding-left:14%;">
                      <div style="margin-left: 15px">
                    <h6>Kreativitas</h6>
                    <h6>Ketelitian</h6>
                    <h6>Sistematika</h6>
                    <h6>Inisiatif</h6>
                    <h6>Tanggung jawab</h6>
                    <h6>Komunikasi</h6>
                    <h6>Penyesuaian diri</h6>
                    <h6>Kerja sama</h6>
                    <h6>Disiplin</h6>
                    <h6>Kehadiran</h6>
                    </div>
                    <div style="margin-left: 20px">
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                      <h6>:</h6>
                    </div>
                    <div style="margin-left: 20px">
                      <h6>{{ $DP->penilaian_dummy->kreativitas }}</h6>
                      <h6>{{ $DP->penilaian_dummy->ketelitian }}</h6>
                      <h6>{{ $DP->penilaian_dummy->sistematika }}</h6>
                      <h6>{{ $DP->penilaian_dummy->inisiatif }}</h6>
                      <h6>{{ $DP->penilaian_dummy->tanggung_jawab }}</h6>
                      <h6>{{ $DP->penilaian_dummy->komunikasi }}</h6>
                      <h6>{{ $DP->penilaian_dummy->penyesuaian_diri }}</h6>
                      <h6>{{ $DP->penilaian_dummy->kerja_sama }}</h6>
                      <h6>{{ $DP->penilaian_dummy->disiplin }}</h6>
                      <h6>{{ $DP->penilaian_dummy->kehadiran }}</h6>
                    </div>
                    </div>
                </div>
                @endforeach
                </div><!--/. container-fluid -->
              </section>
              <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
              <!-- To the right -->
              <div class="float-right d-sm-none d-md-block">
                Anything you want
              </div>
              <!-- Default to the left -->
              <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
            </footer>
            <script>
    function toggler(e) {
        if( e.innerHTML == '<i class="fas fa-eye"></i>' ) {
            e.innerHTML = '<i class="fas fa-eye-slash"></i>'
            document.getElementById('password').type="text";
        } else {
            e.innerHTML = '<i class="fas fa-eye"></i>'
            document.getElementById('password').type="password";
        }
}
</script>

          </div>
          @include('templateAdmin.footer')