@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item" style="border-radius: 5px" >
        <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
          <i class="nav-icon fas fa-sign-out-alt"></i>Logout
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
                <a href="{{ route('dataPesertaAdmin') }}" class="nav-link active">
                  <i class="fas fa-portrait" style="color: black;"></i>
                  <p style="color: black;">Data peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dataTugas') }}" class="nav-link">
                  <i class="fas fa-tasks" style="color: black;"></i>
                  <p style="color: black;">Tugas</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolah') }}" class="nav-link">
              <i class="fas fa-school"></i>
              <p>Sekolah</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Data peserta</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data peserta</li>
            </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">
            <div class="container-fluid">
              @if ($message = Session::get('success'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Success</h5>
                {{ $message }}
              </div>
              @endif
              <a href="{{ route('adminTambahPeserta') }}" >
                <button type="button" class="btn btn-primary col-3" style="margin-bottom: 10px;">
                <i class="fas fa-plus" style="margin-right: 10px;">
                </i>
              Tambah peserta</button></a>
              <form class="search-form col-3" action="{{ route('dataPesertacari') }}" method="GET" style="float: right;">
                <div class="input-group">
                  <input type="text" name="cari" class="form-control" placeholder="Cari nama peserta">
                  <button type="submit" name="submit" class="btn" style="background-color: #5aaaff;border-color: #69c3d1"><i class="fas fa-search" style="color: white;"></i>
                  </button>
                </div>
              </form>
              @if (count($datapeserta))
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="row">
                    <a href="{{ route('kirimpdf') }}" target="_blank">
                      <button type="button" class="btn btn-warning" style=""><i class="far fa-file-pdf" style="margin-right: 10px"></i>Export to PDF</button>
                    </a>
                    <form action="{{ route('filterdatapeserta') }}" method="GET"  style="width: 86%;float: right;">
                    <div class="row">
                      <select class="form-control col-2" style="width: 50%;margin-left: 57%" name="namasekul" value="{{ old('namasekul') }}">
                        <option></option>
                        @foreach ($datasekolah as $ds)
                        <option>{{ $ds->nama_sekolah }}</option>
                        @endforeach
                      </select>
                      <select class="form-control col-2" style="width: 40%;margin-left: 0.5%" name="status" value="{{ old('namasekul') }}">
                        <option></option>
                        <option>Di Terima</option>
                        <option>Di Tolak</option>
                        <option>Belum Di Tentukan</option>
                      </select>
                      <button type="submit" class="btn btn-block btn-secondary btn-sm col-1" style="margin-left: 0.5%" name="submi2">Filter</button>
                    </div>
                    </form>
                  </div>
                  <div class="row">
                    <div class="card-body table-responsive p-0">
                      <table class="table table-hover">
                        <thead>
                          <tr role="row">
                            <th>No</th>
                            <th>Nama</th>
                            <th>Sekolah</th>
                            <th>No telepon</th>
                            <th>Email</th>
                            <th>Tanggal masuk</th>
                            <th>Tanggal keluar</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          @foreach($datapeserta as $no => $DP)
                          <tr role="row">
                            <td>{{ ++$no + ($datapeserta->currentPage()-1) * $datapeserta->perPage() }}</td>
                            <td>{{ $DP->nama}} </td>
                            <td>{{ $DP->sekolah }}</td>
                            <td>{{ $DP->no_tlp}}</td>
                            <td>{{ $DP->email}}</td>
                            <td>{{ $DP->tanggal_masuk }}</td>
                            <td>{{ $DP->tanggal_keluar }}</td>
                            <td>{{ $DP->status }}</td>
                            <td><div class="input-group-prepend">
                              <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="background-color: #17a2b8; color: white">
                              Action
                              </button>
                              <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 48px, 0px);padding: -.5rem 0;">
                                @if ($DP->status == 'Belum Di Tentukan')
                                <form action="{{ url("/admin/Diterima/$DP->NISN") }}" method="POST">
                                  @csrf
                                  <a href="Diterima/{{$DP->NISN}}" style="color: #000">
                                    <button class="dropdown-item" name="update_data_peserta" value="Di Terima" type="submit">Di Terima</button>
                                  </a>
                                </form>
                                <form action="{{ url("/admin/Tidakditerima/$DP->NISN") }}" method="POST">
                                  @csrf
                                  <a href="Tidakditerima/{{$DP->NISN}}" style="color: #000">
                                    <button class="dropdown-item" name="update_data_peserta" value="Di Tolak" type="submit">Di Tolak</button>
                                  </a>
                                </form>
                                <div class="dropdown-divider"></div>
                                @endif
                                <a href="DetailPeserta/{{$DP->NISN}}" style="color: #000"><li class="dropdown-item">Detail peserta</li></a>
                                <a href="Penilaian/{{ $DP->NISN }}" style="color: #000"><li class="dropdown-item">Kelola Nilai</li></a>
                                <a href="editPeserta/{{ $DP->NISN }}" style="color: #000"><li class="dropdown-item">Edit</li></a>
                                <div class="dropdown-divider"></div>
                                <a href="hapus/{{ $DP->NISN }}" style="color: #000" onclick="return confirm('Are you sure?')"><li class="dropdown-item">Hapus</li></a>
                              </ul>
                            </div></td>
                          </tr>
                        </tbody>
                        @endforeach
                      </table>
                    </div>
                  </div>
                  <div style="margin-top: 10px">
                    <div class="dataTables_info" role="status" aria-live="polite" style="float: left;">Showing {{ $datapeserta->firstItem() }} to {{ $datapeserta->lastItem() }} of {{ $datapeserta->total() }} entries</div>
                    <div class="" style="float: right">
                      {{ $datapeserta->appends(['sekolah' => $namasekul,'status' => $status])->links() }}
                    </div>
                  </div>
                  @else
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i> Oops..</h5>
                  Data {{ $cari ?? '' }} tidak ditemukan</div>
                  @endif
                </div>
              </div>
              <!--/. container-fluid -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          <!-- Main Footer -->
          <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-sm-none d-md-block">
              Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
          </footer>
        </div>
        @include('templateAdmin.footer')