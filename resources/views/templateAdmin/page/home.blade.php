@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
                <a href="{{ route('dataPesertaAdmin') }}" class="nav-link">
                  <i class="fas fa-portrait" style="color: black;"></i>
                  <p style="color: black;">Data peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dataTugas') }}" class="nav-link">
                  <i class="fas fa-tasks" style="color: black;"></i>
                  <p style="color: black;">Tugas</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolah') }}" class="nav-link">
              <i class="fas fa-school"></i>
              <p>Sekolah</p>
            </a>
          </li>
           <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">
            <div class="container-fluid">
              <!-- Small boxes (Stat box) -->
              <div class="row">
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>{{ $totalPeserta }}</h3>
                      <p>Jumlah Peserta</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('dataPesertaAdmin') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>{{ $totalSklh }}</h3>
                      <p>Jumlah Sekolah</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('sekolah') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
              </div>
              @if ($message = Session::get('success'))
              <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> Success</h5>
                  {{ $message }}
                </div>
              @endif
              <div class="row">
              <div class="card" style="width: 49.5%;margin-right: 0.5%">
              <div class="card-header border-0">
                <h3 class="card-title">Peserta PKL</h3>
                <div class="card-tools">
                  {{ $pesertatugas->links() }}
                </div>
              </div>
              <div class="card-body p-0">
                <table class="table table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Sekolah</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                @foreach ($pesertatugas as $no => $pst)
                  <tr>
                    <td>{{ ++$no + ($pesertatugas->currentPage()-1) * $pesertatugas->perPage() }}</td>
                    <td>{{ $pst->nama }}</td>
                    <td>{{ $pst->sekolah }}</td>
                    <td>
                      <a href="#" class="text-muted">
                        <button type="button" class="btn btn-block btn-warning btn-sm">Beri tugas</button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
              <div class="card" style="width: 49.5%;margin-left: 0.5%">
              <div class="card-header border-0">
                <h3 class="card-title">Peserta belum ditentukan</h3>
                <div class="card-tools">
                  {{ $pesertabelom->links() }}
                </div>
              </div>
              <div class="card-body p-0">
                <form action="{{ route('konfirmasi') }}" method="POST">
                  {{ csrf_field() }}
                {{ method_field('POST') }}
                <table class="table table-bordered">
                  <thead>
                  <tr>
                    <th>Select</th>
                    <th>Nama</th>
                    <th>Sekolah</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  @foreach ($pesertabelom as $no2 => $psb)
                  <tbody>
                  <tr>
                    <td><input class="form-check-input" type="checkbox" name="check[]" value="{{ $psb->NISN }}" style="margin-left: 2.5%;"></td>
                    <td>{{ $psb->nama }}</td>
                    <td>{{ $psb->sekolah }}</td>
                    <td>
                      <form action="{{ url("/admin/Diterima/$psb->NISN") }}" method="POST">
                      @csrf
                      <a href="Diterima/{{$psb->NISN}}" class="text-muted">
                        <button type="submit" class="btn btn-block btn-success btn-sm" style="width: 48%;float: left;" value="Di Terima" name="update_data_peserta"><i class="fas fa-check"></i></button>
                      </a>
                    </form>
                    <form action="{{ url("/admin/Tidakditerima/$psb->NISN") }}" method="POST">
                      @csrf
                      <a href="Tidakditerima/{{$psb->NISN}}" class="text-muted">
                      <button type="submit" class="btn btn-block btn-danger btn-sm" style="width: 48%;float: right;" value="Di Tolak" name="update_data_peserta"><i class="fas fa-times"></i></button>
                    </a>
                  </form>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                  </div>
                  <div style="margin-bottom: 1%;margin-right: 1%">
                    <div>
                <button type="submit" class="btn btn-block btn-danger btn-sm" style="float: right;width: 20%;" value="Di Tolak" name="update" onclick="return confirm('Are you sure?')"><i class="fas fa-times" style="margin-right: 2px"></i>Selected</button>
                </div>
                <div>
                <button type="submit" class="btn btn-block btn-success btn-sm" style="float: right;width: 20%;margin-right: 5px" value="Di Terima" name="update" onclick="return confirm('Are you sure?')"><i class="fas fa-check" style="padding-right: 2px"></i>Selected</button>
                </div>
                </div>
                </form>
              </div>
          </div>
              <div class="card bg-gradient-success">
                <div class="card-body">
                  <div id="chartDataMasuk"></div>
                </div>
                <!-- /.card-body -->
              </div>
            </div>
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <footer class="main-footer">
          <!-- To the right -->
          <div class="float-right d-sm-none d-md-block">
            Mabar skuii
          </div>
          <!-- Default to the left -->
          <strong>Copyright &copy; 2019 <a href="https://www.arkamaya.co.id/">Arkamaya</a>.</strong> All rights reserved.
        </footer>
      </div>
      <script src="https://code.highcharts.com/highcharts.js"></script>
                  <script>
                    Highcharts.chart('chartDataMasuk', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Siswa'
    },
    subtitle: {
        text : '{{$month}}'
    },
    xAxis: {
        categories: [
            'Bulan ini'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah peserta'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Total',
        data: [{{$totalpesertabulan}}]

    }, {
        name: 'Belum ditentukan',
        data: [{{$totalbelumditentukan}}]

    }, {
        name: 'Diterima',
        data: [{{$totalditerima}}]

    }, {
        name: 'Ditolak',
        data: [{{$totaltidakditerima}}]

    }]
});
                  </script>
      @include('templateAdmin.footer')