@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
                <a href="{{ route('dataPesertaAdmin') }}" class="nav-link active">
                  <i class="fas fa-portrait" style="color: black;"></i>
                  <p style="color: black;">Data peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dataTugas') }}" class="nav-link">
                  <i class="fas fa-tasks" style="color: black;"></i>
                  <p style="color: black;">Tugas</p>
                </a>
              </li>
               <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolah') }}" class="nav-link">
              <i class="fas fa-school"></i>
              <p>Sekolah</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Penilaian</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="">Peserta</a></li>
                <li class="breadcrumb-item active">Penilaian</li>
              </ol>
              </div><!-- /.col -->
              </div><!-- /.row -->
              </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
              <form action="{{ route('penilaianfungsi')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="container-fluid">
                  <div class="row" style="margin-bottom: 10px;">
                    <div class="col-3">
                      <button type="button" class="btn btn-block btn-primary" style="">
                      <a href="{{ route('dataPesertaAdmin')}}" style="color: white">
                        <i class="fas fa-arrow-left" style="margin-right: 10px;color: white;"></i>Kembali</button>
                      </a>
                    </div>
                    <div class="col-3" style="margin-left: 540px">
                      <button class="btn btn-block btn-primary" style="" type="submit">
                      <i class="far fa-save" style="margin-right: 10px;"></i>Save penilaian</button>
                    </div>
                  </div>
                  <!-- Info boxes -->
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          <strong>Perhatian!!!</strong><br>
                          <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        @endif
                            <div class="card-body">
                              @foreach($datapeserta as $DP)
                              <div class="row">
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="nisn">NISN</label>
                                <input type="number" class="form-control" id="nisn1" name="nisn1" value="{{ $DP->NISN }}" disabled="">
                                <input type="hidden" class="form-control" id="nisn" name="nisn" value="{{ $DP->NISN }}">
                              </div>
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="nisn">Kreativitas</label>
                                <input type="number" class="form-control" id="kreativitas" name="kreativitas" value="{{ $DP->penilaian_dummy->kreativitas }}">
                              </div>
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Ketelitian</label>
                                <input type="number" class="form-control" name="ketelitian" id="ketelitian" value="{{ $DP->penilaian_dummy->ketelitian }}">
                              </div>
                              </div>
                              <div class="row">
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Sistematika</label>
                                <input type="number" class="form-control" name="sistematika" id="sistematika" value="{{ $DP->penilaian_dummy->sistematika }}">
                              </div>
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Inisiatif</label>
                                <input type="number" class="form-control" name="inisiatif" id="inisiatif" value="{{ $DP->penilaian_dummy->inisiatif }}">
                              </div>
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Tanggung jawab</label>
                                <input type="number" class="form-control" name="tanggungjawab" id="tanggungjawab" value="{{ $DP->penilaian_dummy->tanggung_jawab }}">
                              </div>
                              </div>
                              <div class="row">
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Komunikasi</label>
                                <input type="number" class="form-control" name="komunikasi" id="komunikasi" value="{{ $DP->penilaian_dummy->komunikasi }}">
                              </div>
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Penyesuaian diri</label>
                                <input type="number" class="form-control" name="penyesuaiandiri" id="penyesuaiandiri" value="{{ $DP->penilaian_dummy->penyesuaian_diri }}">
                              </div>
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Kerja sama</label>
                                <input type="number" class="form-control" name="kerjasama" id="kerjasama" value="{{ $DP->penilaian_dummy->kerja_sama }}">
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-3" style="margin-right: 10px">
                                <label for="">Disiplin</label>
                                <input type="number" class="form-control" name="disiplin" id="disiplin" value="{{ $DP->penilaian_dummy->disiplin }}">
                              </div>
                                <div class="form-group col-3" style="margin-right: 10px">
                                  <label for="">Kehadiran</label>
                                  <input type="number" class="form-control" name="kehadiran" id="kehadiran" value="{{ $DP->penilaian_dummy->kehadiran }}">

                                  <input type="hidden" name="id" value="{{ $DP->penilaian_dummy->id_penilaian }}">
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                        </div>
                        <!-- /.row -->
                    </div><!--/. container-fluid -->
                  </form>
                </section>
                <!-- /.content -->
              </div>
              <!-- /.content-wrapper -->
              <!-- Main Footer -->
              <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-sm-none d-md-block">
                  Anything you want
                </div>
                <!-- Default to the left -->
                <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
              </footer>
            </div>
            <!-- ./wrapper -->
            <script>
            let preview = function(event) {
            let imageSection = document.getElementById('imageUploadSection');
            let imageUpload = document.getElementById('fotoBarang');
            let template = function(src) {
            var image = new Image();
            image.classList.add('img-thumbnail');
            image.classList.add('col-4');
            image.classList.add('fotoBarang');
            image.src = src;
            return image;
            }
            let openFile = function(file) {
            let reader = new FileReader();
            reader.onload = function() {
            let dataURL = this.result;
            imageSection.appendChild(template(dataURL));
            };
            reader.readAsDataURL(file);
            
            }
            if(imageUpload.files) {
            imageSection.innerHTML = "";
            [].forEach.call(imageUpload.files, openFile);
            imageUpload.files.forEach(console.log(imageUpload.files));
            }
            }
            </script>
            @include('templateAdmin.footer')