@include('templateAdmin.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-users"></i>
              <p class="text">Peserta
              <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="background-color: grey;">
              <li class="nav-item">
                <a href="{{ route('dataPesertaAdmin') }}" class="nav-link">
                  <i class="fas fa-portrait" style="color: black;"></i>
                  <p style="color: black;">Data peserta</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('dataTugas') }}" class="nav-link">
                  <i class="fas fa-tasks" style="color: black;"></i>
                  <p style="color: black;">Tugas</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('sekolah') }}" class="nav-link active">
              <i class="fas fa-school"></i>
              <p>Sekolah</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Data sekolah</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">sekolah </li>
            </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <!-- Main content -->
          
          <section class="content">
            <div class="container-fluid">
              <button type="button" class="btn btn-block btn-primary" style="width: 15%;margin-bottom: 10px;"><a href="{{ route('tambah_Sekolah') }}" style=" color: white"><i class="fas fa-plus" style="margin-right: 10px;"></i>Tambah sekolah</a></button>
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <table class="table table-condensed">
                      <tr>
                        <th class="text-center">Foto</th>
                        {{-- <th>ID Sekolah</th> --}}
                        <th>Nama Sekolah</th>
                        <th>Alamat</th>
                        <th>Nama Pembimbing</th>
                        <th>Nomor Telephone</th>
                        <th class="text-center" style="width: 200px">Action</th>
                      </tr>
                      @if (!empty($datasekolah))
                      <?php $i = 1; ?>
                      @foreach ($datasekolah as $data)
                      <tr>
                        <td style="width: 114px"><img style="width: 75px;height: 75px;object-fit: cover; border-radius: 10px;" src="{{URL::to($data->foto_sekolah)}}"></td>
                        {{-- <td>{{ $data->id_sekolah }}</td> --}}
                        <td>{{ $data->nama_sekolah }}</td>
                        <td>{{ $data->alamat_sekolah }}</td>
                        <td>{{ $data->nama_pembimbing }}</td>
                        <td>{{ $data->no_tlp }}</td>
                        <td class="text-center">
                          <div class="btn-group">
                            <button type="button" class="btn btn-info">Action</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            </button>
                            <div class="dropdown-menu" role="menu" style="margin-left: -80px">
                              <a class="dropdown-item" href="editSekolah/{{ $data->id_sekolah }}">Edit</a>
                              <form action="{{ url("admin/sekolah/$data->id_sekolah") }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="dropdown-item">Delete</button>
                              </form>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <?php $i++; ?>
                      @endforeach
                      @endif
                    </table>
                  </div>
                  <div style="">
                    <div class="dataTables_info" role="status" aria-live="polite" style="float: left;">Showing {{ $datasekolah->firstItem() }} to {{ $datasekolah->lastItem() }} of {{ $datasekolah->total() }} entries</div>
                    <div class="" style="float: right">
                      {{ $datasekolah->links() }}
                    </div>
                  </div>
                </div>
              </div>
              </div><!--/. container-fluid -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          <!-- Main Footer -->
          <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-sm-none d-md-block">
              Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
          </footer>
        </div>
        @include('templateAdmin.footer')