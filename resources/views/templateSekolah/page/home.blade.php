@include('templateSekolah.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item" style="border-radius: 5px" >
        <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
          <i class="nav-icon fas fa-sign-out-alt"></i>Logout
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{URL::to($user->sekolah_dummy->foto_sekolah)}}" class="img-circle elevation-2" alt="{{URL::to($user->sekolah_dummy->foto_sekolah)}}">
        </div>
        <div class="info">
          <a href="#" class="d-block" name="nama_sekolah">{{ $user->name }}</a>
          <a href="#" class="d-block" name="nama_sekolah">{{ $user->email }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('sekolahHome') }}" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('sekolahEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard Sekolah</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">
            <div class="container-fluid">
              <!-- Info boxes -->
              <div class="row">
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>{{ $totalPeserta }}</h3>
                      <p>Jumlah Peserta</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-bag"></i>
                    </div>
                    
                  </div>
                </div>
                <!-- ./col -->
              </div>
              <div class="row">
                
                <div class="container-fluid">
                   @if ($message = Session::get('success'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Success</h5>
                {{ $message }}
              </div>
              @endif
                  <a href="{{ route('tambahpeserta') }}" >
                <button type="button" class="btn btn-primary col-3" style="margin-bottom: 10px;">
                <i class="fas fa-plus" style="margin-right: 10px;">
                </i>
              Tambah peserta</button></a>
                  <form class="search-form col-3" action="{{ route('dataPesertacariS') }}" method="GET" style="float: right;">
                    <div class="input-group">
                      <input type="text" name="cari" class="form-control" placeholder="Cari nama peserta">
                      <button type="submit" name="submit" class="btn" style="background-color: #5aaaff;border-color: #69c3d1"><i class="fas fa-search" style="color: white;"></i>
                      </button>
                    </div>
                </form>
              </div>
            </div>
                @if (count($datapeserta))
                <div class="card">
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="card-body table-responsive p-0">
                        <table id="example2" class="table table-hover">
                          <thead>
                            <tr role="row">
                              <th>Foto</th>
                              <th>Nama</th>
                              <th>Sekolah</th>
                              <th>No telepon</th>
                              <th>Email</th>
                              <th>Tanggal masuk</th>
                              <th>Tanggal keluar</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            
                            @foreach($datapeserta as $DP)
                            <tr role="row">
                              <td style="width: 114px"><img style="width: 100px;height: 125px;object-fit: cover;" src="{{URL::to($DP->foto)}}"></td>
                              <td>{{ $DP->nama}} </td>
                              <td>{{ $DP->sekolah }}</td>
                              <td>{{ $DP->no_tlp}}</td>
                              <td>{{ $DP->email}}</td>
                              <td>{{ $DP->tanggal_masuk }}</td>
                              <td>{{ $DP->tanggal_keluar }}</td>
                              <td><div class="input-group-prepend">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="background-color: #17a2b8; color: white">
                                Action
                                </button>
                                <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 48px, 0px);">
                                  <li class="dropdown-item"><a href="#" stHyle="color: #000">Lihat nilai</a></li>
                                  <li class="dropdown-item"><a href="editPeserta/{{ $DP->NISN }}" style="color: #000">Edit</a></li>
                                  <a href="hapus/{{ $DP->NISN }}/{{ $DP->email }}" style="color: #000"><li class="dropdown-item">Hapus</li></a>
                                  <li class="dropdown-item">
                                    <form action="{{ url("sekolah/Home/$DP->NISN")}}" method="POST">
                                      
                                      {{ csrf_field() }}
                                    </form>
                                  </li>
                                </ul>
                              </div></td>
                            </tr>
                          </tbody>
                          @endforeach
                        </table>
                      </div>
                    </div>
                    <div style="">
                      <div class="dataTables_info" role="status" aria-live="polite" style="float: left;">Showing {{ $datapeserta->firstItem() }} to {{ $datapeserta->lastItem() }} of {{ $datapeserta->total() }} entries</div>
                      <div class="" style="float: right">
                        {{ $datapeserta->links() }}
                      </div>
                    </div>
                    @else
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h5><i class="icon fas fa-ban"></i> Ops..</h5>
                    Data {{ $cari ?? '' }} tidak ditemukan</div>
                    @endif
                  </div>
                </div>
              </section>
              </div><!--/. container-fluid -->
              
              <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
              <!-- To the right -->
              <div class="float-right d-sm-none d-md-block">
                Anything you want
              </div>
              <!-- Default to the left -->
              <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
            </footer>
          </div>
          @include('templateSekolah.footer')