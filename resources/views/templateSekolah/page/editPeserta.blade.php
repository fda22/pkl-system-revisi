@include('templateSekolah.header')
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item" style="border-radius: 5px" >
           <a href="{{ route('adminLogout') }}" class="nav-link-logout nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>Logout
          </a>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4"style="background-color: black;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style=" background-color: black;">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyJepySjOVaIUZqbkrL-u6ftkYEAvPOtytaiqbpSLXLaxWK8ni" alt="" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">ARKAMAYA</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar"style="padding-left: 0rem; padding-right: 0rem; background-color: black;" >
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-business-male-icon-vector-png-image_916468.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('sekolahHome') }}" class="nav-link active">
              <i class="fas fa-home"></i>
              <p class="text">Home</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('sekolahEditProfile') }}" class="nav-link">
              <i class="fas fa-edit"></i>
              <p class="text">Edit Profile</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit peserta</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="">Peserta</a></li>
                <li class="breadcrumb-item active">Tambah peserta</li>
              </ol>
              </div><!-- /.col -->
              </div><!-- /.row -->
              </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            @foreach($datapeserta as $DP)
            <section class="content">
              <form action="updatePeserta/{{ $DP->NISN }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="container-fluid">
                  <div class="row" style="margin-bottom: 10px;">
                    <div class="col-3">
                      <a href="{{ route('sekolahHome')}}" style="color: white">
                      <button type="button" class="btn btn-block btn-primary" style="">
                        <i class="fas fa-arrow-left" style="margin-right: 10px;color: white;"></i>Kembali</button>
                      </a>
                    </div>
                    <div class="col-3" style="margin-left: 540px">
                      <button class="btn btn-block btn-primary" style="" type="submit">
                      <i class="far fa-save" style="margin-right: 10px;"></i>Save peserta</button>
                    </div>
                  </div>
                  <!-- Info boxes -->
                  <div class="card">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Perhatian!!!</strong><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    @endif
                    <div class="card-body">
                      <div class="row">
                        <div class="form-group col-4">
                          
                          <input type="number" class="form-control" id="nisn" placeholder="Masukkan NISN" name="nisn" value="{{ $DP->NISN}}" hidden="">
                          <label for="no_tlp" style="margin-top: 15px">No telepon</label>
                          <input type="number" class="form-control" id="notlp" placeholder="Masukkan no telepon" name="notlp" value="{{ $DP->no_tlp}}">
                          <label for="password" style="margin-top: 15px">Password</label>
                          <div class="input-group mb-3">
                          <input type="Password" class="form-control" placeholder="Masukkan password" name="password" id="password" value="{{ $DP->password}}"><button onclick="toggler(this)" type="button" style="background-color:  white ; border-style: none;"><i class="fas fa-eye"></i></button>
                        </div>
                          <label for="tanggalmasuk">Tanggal masuk</label>
                          <input type="date" class="form-control" placeholder="" name="tanggalmasuk" id="tanggalmasuk" value="{{ $DP->tanggal_masuk }}">
                          <label for="tanggalkeluar"  style="margin-top: 15px">Tanggal keluar</label>
                          <input type="date" class="form-control" placeholder="" name="tanggalkeluar" id="tanggalkeluar" value="{{ $DP->tanggal_keluar }}">
                        </div>
                        <div class="form-group col-4">
                          <label for="namapeserta"  style="margin-top: 15px">Nama peserta</label>
                          <input type="text" class="form-control" placeholder="Masukkan nama peserta" name="namapeserta" id="namapeserta" value="{{ $DP->nama}}">
                          <label for="sekolah"  style="margin-top: 15px">Sekolah</label>
                          <input type="text" class="form-control" placeholder="Masukkan asal sekolah" name="sekolah" id="sekolah" value="{{ $DP->sekolah}}">
                          <label for="email" style="margin-top: 15px">Email</label>
                          <input type="text" class="form-control" placeholder="Masukkan email peserta" name="email" id="email" value="{{ $DP->email}}">
                        </div>
                        <div class="form-group" style="margin-left: 25px">
                          <label for="foto">Foto </label>
                          <p><img src="{{URL::to($DP->foto)}}" id="image-preview" alt="jir" style="width: 100px;height: 125px;object-fit: cover;"></p>
                          <input type="file" name="foto" id="image-source" onchange="previewImage();" value="{{ $DP->foto }}">

                          <p>*Pas foto berukuran 3x4</p>
                          @endforeach
                        </div>
                      </div>
                      <!-- /.row -->
                      </div><!--/. container-fluid -->
                    </form>
                  </section>
                      <!-- /.content -->
                    </div>
                    <!-- /.content-wrapper -->
                    <!-- Main Footer -->
                    <footer class="main-footer">
                      <!-- To the right -->
                      <div class="float-right d-sm-none d-md-block">
                        Anything you want
                      </div>
                      <!-- Default to the left -->
                      <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
                    </footer>
                  </div>
                  <!-- ./wrapper -->
                  <script>
                  function previewImage() {
                  document.getElementById("image-preview");
                  var oFReader = new FileReader();
                  oFReader.readAsDataURL(document.getElementById("image-source").files[0]);
                  
                  oFReader.onload = function(oFREvent) {
                  document.getElementById("image-preview").src = oFREvent.target.result;
                  };
                  };
                  function toggler(e) {
        if( e.innerHTML == '<i class="fas fa-eye"></i>' ) {
            e.innerHTML = '<i class="fas fa-eye-slash"></i>'
            document.getElementById('password').type="text";
        } else {
            e.innerHTML = '<i class="fas fa-eye"></i>'
            document.getElementById('password').type="password";
        }
}
                  </script>
                  @include('templateSekolah.footer')