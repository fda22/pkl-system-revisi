<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Penilaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian', function (Blueprint $table) {
            $table->bigIncrements('id_penilaian');
            $table->string('NISN')->unique();
            $table->integer('kreativitas')->nullable();
            $table->integer('ketelitian')->nullable();
            $table->integer('sistematika')->nullable();
            $table->integer('inisiatif')->nullable();
            $table->integer('tanggung_jawab')->nullable();
            $table->integer('komunikasi')->nullable();
            $table->integer('penyesuaian_diri')->nullable();
            $table->integer('kerja_sama')->nullable();
            $table->integer('disiplin')->nullable();
            $table->integer('kehadiran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
