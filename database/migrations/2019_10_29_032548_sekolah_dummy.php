<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SekolahDummy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolah_dummy', function (Blueprint $table) {
            $table->string('id_sekolah')->primary();
            $table->foreign('id_sekolah')->references('id_sekolah')->on('users');
            $table->string('nama_sekolah');
            $table->string('alamat_sekolah');
            $table->string('nama_pembimbing');
            $table->string('no_tlp');
            $table->string('foto_sekolah');
            $table->string('email')->unique();
            $table->string('password');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolah_dummy');
    }
}
