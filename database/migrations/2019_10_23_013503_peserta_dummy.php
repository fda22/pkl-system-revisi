<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PesertaDummy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_dummy', function (Blueprint $table) {
            $table->string('NISN')->primary();
            $table->foreign('NISN')->references('NISN')->on('users');
            $table->string('foto');
            $table->string('no_tlp');
            $table->string('password');
            $table->string('nama', 50);
            $table->string('sekolah', 50);
            $table->string('email');
            $table->date('tanggal_masuk');
            $table->date('tanggal_keluar');
            $table->enum('status' , ['Di Terima','Di Tolak', 'Belum Di Tentukan']);
            $table->timestamps();
            $table->tinyInteger('id_kehadiran')->unique()->nullable();
            $table->bigInteger('id_penilaian')->unique()->nullable();
            $table->bigInteger('id_tugas')->unique()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta_dummy');
    }
}
