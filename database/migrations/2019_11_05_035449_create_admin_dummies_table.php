<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminDummiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_dummy', function (Blueprint $table) {
            $table->string('id_admin')->primary();
            $table->foreign('id_admin')->references('id_admin')->on('users');
            $table->string('nama_perusahaan');
            $table->string('nama_admin');
            $table->string('logo_perusahaan');
            $table->string('foto_admin');
            $table->string('alamat_perusahaan');
            $table->string('notelp_perusahaan');
            $table->string('email')->unique();
            $table->string('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_dummy');
    }
}
