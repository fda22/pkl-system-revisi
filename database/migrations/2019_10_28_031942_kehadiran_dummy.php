<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KehadiranDummy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kehadiran_dummy', function (Blueprint $table) {
            $table->tinyIncrements('id_kehadiran');
            $table->string('NISN');
            $table->foreign('NISN')->references('NISN')->on('peserta_dummy');
            $table->enum('kehadiran' , ['Hadir','Izin','Sakit','Alfa']);
            $table->string('keterangan');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kehadiran_dummy');
    }
}
