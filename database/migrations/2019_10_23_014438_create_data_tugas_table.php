<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_tugas', function (Blueprint $table) {
            $table->bigIncrements('id_tugas');
            $table->string('NISN');
            $table->foreign('NISN')->references('NISN')->on('peserta_dummy');
            $table->string('tugas');
            $table->date('tanggal_diberikan');
            $table->enum('status' , ['Di Kerjakan','Belum Di Kerjakan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_tugas');
    }
}
