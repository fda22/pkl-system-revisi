<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', 'AdminController@index')->name('adminHome');
// Route::get('/logout', 'AdminController@logout')->name('adminLogout');
// 
// Route::get('/search', 'UserController@search');
// Route::get('/barang/{id}', 'UserController@single');

// Auth::routes(['verify' => false]);
Auth::routes();

Route::group(['admin','middleware' => ['web', 'auth']], function (){

   
        Route::get('/', function(){
        if (Auth::user()->admin==0) {
            $user = Auth::user();
            return redirect()->action('UserController@indexUser');
        }
        if (Auth::user()->admin==1) {
            $user = Auth::user();
            return redirect()->action('AdminController@indexAdmin');
        }
        if (Auth::user()->admin==2) {
            $user = Auth::user();
            return redirect()->action('SekolahController@indexSekolah');
            }
        });
});

// admin route
Route::prefix('admin')->group(function() {
    //perusahaan
        Route::get('/Home', 'AdminController@indexAdmin')->name('adminHome');
        Route::get('/DataTugas', 'AdminController@dataTugas')->name('dataTugas');
        Route::get('/DataPeserta', 'AdminController@dataPeserta')->name('dataPesertaAdmin');
        Route::get('/hapus/{NISN}','AdminController@hapusPeserta')->name('hapusdataPeserta');
        Route::get('/DataPeserta/tambahpeserta', 'AdminController@tambahPeserta')->name('adminTambahPeserta');
        Route::post('/admin/DataPeserta/simpanPeserta','AdminController@tambahdatapeserta')->name('tambahdatapeserta');
        Route::get('/DetailPeserta/{id}', 'AdminController@detailPeserta')->name('detailPeserta');
        Route::get('/cari', 'AdminController@dataPesertacari')->name('dataPesertacari');
        Route::get('filter','AdminController@filter')->name('filterdatapeserta');
        Route::get('/editPeserta/{id}', 'AdminController@editPeserta')->name('editPeserta');
        Route::post('/editPeserta/updatePeserta/{NISN}','AdminController@editPesertaFungsi')->name('editPesertaFungsi');
        Route::get('/TambahTugas', 'AdminController@tambah_tugas')->name('tambah_tugas');
        Route::get('/EditTugas/{id}', 'AdminController@edit_tugas')->name('edit_tugas');
        Route::post('/TambahTugas', 'AdminController@tambah_tugas_fungsi')->name('tambah_tugas_fungsi');
        Route::post('/EditTugas', 'AdminController@edit_tugas_fungsi')->name('edit_tugas_fungsi');        
        Route::delete('/DataTugas/{id}', 'AdminController@hapus_data_tugas')->name('hapus_data_tugas');
        Route::get('/TambahNilai', 'AdminController@tambahnilai')->name('tambahNilai');
        Route::get('/sekolah', 'AdminController@sekolah')->name('sekolah');
        Route::get('/tambahSekolah', 'AdminController@tambah_Sekolah')->name('tambah_Sekolah');
        Route::post('/tambahSekolah', 'AdminController@tambah_sekolah_fungsi')->name('tambah_sekolah_fungsi');
        Route::post('/editSekolah', 'AdminController@edit_sekolah_fungsi')->name('edit_sekolah_fungsi');
        Route::get('/editSekolah/{id}', 'AdminController@edit_sekolah')->name('edit_sekolah');
        Route::delete('/sekolah/{id}', 'AdminController@hapus_data_sekolah')->name('hapus_data_sekolah');
        Route::get('/Penilaian/{id}','AdminController@penilaian')->name('penilaian');
        Route::post('/Penilaian', 'AdminController@penilaianfungsi')->name('penilaianfungsi');
        Route::get('/EditProfile', 'AdminController@edit_profile_admin')->name('adminEditProfile');
        //Diterima satu satu
        Route::post('/Diterima/{NISN}','AdminController@diterima')->name('diterima');
        Route::get('/Diterima/{NISN}','AdminController@diterima')->name('diterima');
        //Diterima multiply
        Route::post('/Konfirmasi','AdminController@accepted')->name('konfirmasi');
        Route::get('/Konfirmasi','AdminController@accepted')->name('konfirmasi');


        Route::get('Tidakditerima/{NISN}','AdminController@tidakditerima')->name('tidakditerima');
        Route::post('Tidakditerima/{NISN}','AdminController@tidakditerima')->name('tidakditerima');
        Route::get('/KirimPDF','AdminController@kirimpdf')->name('kirimpdf');

    });

Route::prefix('user')->group(function() {
    //user
        Route::get('/Home', 'UserController@indexUser')->name('indexUser');
        Route::get('/tugas', 'UserController@tugas')->name('userTugas');
        Route::get('/editProfile', 'UserController@edit_profile')->name('userEditProfile');
        Route::post('/tugas{id}', 'UserController@update_data_tugas')->name('update_data_tugas');
        Route::post('/Home', 'UserController@tambah_kehadiran')->name('tambah_kehadiran');
        Route::post('/editProfile','UserController@editPesertaFungsi')->name('editPesertaFungsiU');

     });

     Route::prefix('sekolah')->group(function() {
        //sekolah
            Route::get('/Home', 'SekolahController@indexSekolah')->name('sekolahHome');
            Route::get('/cari', 'SekolahController@dataPesertacari')->name('dataPesertacariS');
            Route::get('/Home/tambahPeserta', 'SekolahController@tambahPeserta')->name('tambahpeserta');
            Route::post('/Home/simpanPeserta','SekolahController@tambahdatapeserta')->name('tambahdatapesertaS');
            Route::get('/editPeserta/{id}', 'SekolahController@editPeserta')->name('editPesertaSekolah');
            Route::post('/editPeserta/updatePeserta/{NISN}','SekolahController@editPesertaFungsi')->name('editPesertaFungsiS');
            Route::get('/hapus/{NISN}/{email}','SekolahController@hapusPeserta')->name('hapusdataPesertaS');
            Route::get('/EditProfile', 'SekolahController@edit_profile_sekolah')->name('sekolahEditProfile');
            Route::post('/EditProfile', 'SekolahController@edit_sekolah_fungsi')->name('editsekolahfungsi');

         });
    //perusahaan
    // Route::get('/home', 'AdminController@indexAdmin')->name('adminHome');
    // Route::get('/DataTugas', 'AdminController@dataTugas')->name('dataTugas');
    // Route::get('/DataPeserta', 'AdminController@dataPeserta')->name('dataPesertaAdmin');
    // Route::get('/DetailPeserta', 'AdminController@detailPeserta')->name('detailPeserta');
    Route::get('/logout', 'AdminController@logout')->name('adminLogout');
    // Route::get('/pengiriman', 'AdminController@pengiriman')->name('adminPengiriman');
    // Route::get('/kategori', 'AdminController@kategori_barang')->name('adminKategoriBarang');

    // Route::post('/pengiriman', 'AdminController@tambah_jasa_pengiriman_controller')->name('adminTambahJasaPengirimanController');
    // Route::get('/barang', 'AdminController@barang')->name('adminBarang');
    // Route::get('/barang/tambahBarang', 'AdminController@tambah_barang')->name('adminTambahBarang');
    // Route::post('/barang', 'AdminController@tambah_barang_controller')->name('adminTambahBarangController');
    // Route::get('/barang/{id}/detail', 'AdminController@detail_barang_controller')->name('adminDetailBarangController');
    // Route::get('/barang/{id}/edit', 'AdminController@edit_barang')->name('adminEditBarang');
    // Route::patch('/barang/{id}', 'AdminController@edit_barang_controller')->name('adminEditBarangController');
    // Route::delete('/barang/{id}', 'AdminController@hapus_barang_controller')->name('adminHapusBarangController');
    // Route::delete('/pengiriman/{id}', 'AdminController@hapus_jasa_pengiriman_controller')->name('adminHapusJasaPengirimanController');
    // Route::post('/pengiriman/{id}/edit', 'AdminController@edit_jasa_pengiriman_controller')->name('adminEditJasaPengirimanController');
    // Route::post('/kategori', 'AdminController@tambah_kategori_barang')->name('adminTambahKategoriBarang');
    // Route::delete('/kategori/{id}', 'AdminController@hapus_kategori_barang')->name('adminHapusKategoriBarang');
    // Route::post('/kategori/{id}/edit', 'AdminController@edit_kategori_barang')->name('adminEditKategoriBarang');



Route::get('/home', 'HomeController@index')->name('home');
